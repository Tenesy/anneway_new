<div class="content-box mb-50">
    <h4><strong>Категории</strong></h4>
    <ul class="list-unstyled padding-x2-list separate-list">
        <?php $__currentLoopData = $categorys; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16" width="16" class="icon svg-secondary icon-pos-left"><path d="m0 0h16v16h-16zm2 14h12v-12h-12zm2-9v-1h2v1c0 1.44771525.55228475 2 2 2s2-.55228475 2-2v-1h2v1c0 2.55228475-1.4477153 4-4 4-2.55228475 0-4-1.44771525-4-4z"></path></svg><a href="/blog/<?php echo e($category->id); ?>"><?php echo e($category->name); ?></a> <span class="float-right"><?php echo e($category->count_article()); ?></span></li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    </ul>
</div>
<?php /**PATH /Users/serezamakar/Desktop/anneway_new/resources/views/layouts/blog/block/category.blade.php ENDPATH**/ ?>