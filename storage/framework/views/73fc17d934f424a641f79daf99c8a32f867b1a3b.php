<?php $__env->startSection('content'); ?>
<section id="subscribe-block" class="pt-50 pb-50 pt-md-100 pb-md-100 text-center light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
                <div class="content-box padding-x3 bg-default shadow" style="box-shadow: 0px 8px 20px 0px rgb(0 0 0 / 20%);">
                    <h3 class="mb-20"><?php if(!$err): ?><?php echo e(__('index.check_email')); ?><?php endif; ?></h3>
                    <p class="text-secondary"><?php if(!$err): ?><?php echo e(__('index.link_email')); ?> <?php echo e($email); ?><?php else: ?><span style="font-size:20px"><?php echo e($err); ?></span>  <?php endif; ?></p>
                    <?php if($err): ?><p style="font-size:20px" class="text-primary"><a class="col-md-6" href="/login">вход</a> <a class="col-md-6" href="/register">регистрация</a></p><?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('welcome', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/serezamakar/Desktop/anneway_new/resources/views/layouts/info-block.blade.php ENDPATH**/ ?>