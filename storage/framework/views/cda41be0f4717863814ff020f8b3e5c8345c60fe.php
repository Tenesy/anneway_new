<?php $__env->startSection('content'); ?>
<section id="inner-post-sidebar" class="pt-50 pt-md-100 pb-md-50 light">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <h3><?php echo e(__('thours.excursion_list')); ?></h3>
            </div>
            <!-- Список экскурсий -->
            <div class="col-md-8 col-lg-9">
                <!-- Один элемент списка  -->
                <div class="content-box d-sm-flex mb-75 text-left">
                    <button type="button" class="btn btn-primary"><a href="/tours/create">добавить</a></button>
                </div>
                <?php $__currentLoopData = $tours; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tour): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="content-box d-sm-flex mb-75 text-left">
                        <div class="mr-sm-50 mb-30 mb-sm-0">
                            <a href="#"><img width="150" height="150" src="<?php if(count($tour->photos)): ?><?php echo e($tour->photos[0]->photo); ?> <?php else: ?><?php echo e('https://via.placeholder.com/150'); ?><?php endif; ?>" alt="image"></a>
                        </div>
                        <div>
                            <div class="row no-gutters d-flex justify-content-between">
                                <h4><strong><?php echo e($tour->title); ?></strong></h4>
                                <div class="status status-wait">В ожидании</div>
                            </div>
                            <p class="text-secondary small"><?php echo e($tour->created_at->format('d-m-w')); ?> | <a href="#"><?php echo e($tour->city); ?></a></p>
                            <p class="mb-30"><?php echo e($tour->description); ?></p>

                            <ul class="list-inline small d-flex justify-content-between">
                                <div class="row no-gutters">
                                    <li><a href="#" class="btn btn-primary"><strong>Остановить</strong></a></li>
                                    <li><a href="#" class="btn btn-primary ml-2"><strong>Редактировать</strong></a></li>
                                </div>
                                <div>
                                    <li onclick="$('#popup-subscribe').modal()"><a href="#" class="ml-2 btn btn-primary"><strong>На модерацию</strong></a></li>
                                </div>
                            </ul>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



            </div>
            <!-- Боковая панель -->
            <aside class="col-md-4 col-lg-3">
                <form action="scripts/request.php" class="contact_form form-inline mb-50" id="inner-post-sidebar-form" novalidate="novalidate">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Поиск" name="SEARCH" size="10"></div>
                    <button type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-link"><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16" width="16" class="icon svg-default"><path d="m12.4199285 10.8391071 3.2713969 3.2713969c.4115661.4115661.4115661 1.0788466 0 1.4904127s-1.0788466.4115661-1.4904127 0l-3.2573956-3.2573956c-1.14199306.8523301-2.55871405 1.3569312-4.09329095 1.3569312-3.78327543 0-6.85022615-3.0669507-6.85022615-6.85022615 0-3.78327543 3.06695072-6.85022615 6.85022615-6.85022615 3.78327545 0 6.85022615 3.06695072 6.85022615 6.85022615 0 1.48814524-.4745276 2.86545861-1.2805238 3.98888095zm-5.56970235.7535833c2.61919069 0 4.74246425-2.12327356 4.74246425-4.74246425 0-2.61919068-2.12327356-4.74246426-4.74246425-4.74246426-2.61919068 0-4.74246426 2.12327358-4.74246426 4.74246426 0 2.61919069 2.12327358 4.74246425 4.74246426 4.74246425z"></path></svg></button>
                </form>

                <div class="content-box mb-50">
                    <h4><strong><?php echo e(__('thours.help')); ?></strong></h4>
                    <ul class="list-unstyled padding-x2-list separate-list">
                        <li><a href="#"><?php echo e(__('thours.experiences_travelers')); ?></a></li>
                        <li><a href="#"><?php echo e(__('thours.add_ararticle')); ?></a></li>
                        <li><a href="#"><?php echo e(__('thours.moderated_content')); ?></a></li>
                    </ul>
                </div>

                <div class="content-box mb-50">
                    <h4 class="mb-20"><strong><?php echo e(__('thours.tag_cloud')); ?></strong></h4>
                    <div class="btn-cloud">
                        <a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a>
                    </div>
                </div>

            </aside>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('welcome', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/serezamakar/Desktop/anneway_new/resources/views/layouts/tours/list.blade.php ENDPATH**/ ?>