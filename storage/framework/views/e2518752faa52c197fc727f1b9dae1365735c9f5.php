<div class="chat__not-selected">
    <div class="text-center">
        <i class="fa fa-comments font-4xl" aria-hidden="true"></i>
    </div>
    <?php echo e(__('messages.no_conversation_selected_yet')); ?>

</div>
<?php /**PATH /Users/serezamakar/Desktop/anneway_new/resources/views/chat/no-chat.blade.php ENDPATH**/ ?>