<?php
return [
    'header' => [
        'home' => 'Главная',
        'tours' => 'Экскурсии',
        'blog' => 'Блог',
        'news' => 'Новости',
        'about_us' => 'О нас'
    ],

    "login" => "Войти",
    "register" => "Регистрация",

    'user' => 'Пользователь',
    'Messages' => 'Сообщения',
    'Orders' => 'Заказы',
    'Calendar' => 'Календарь',
    'Profile' => 'Профиль',
    'Settings' => 'Настройки',
    'Output' => 'Выход',
    'My_excursions' => 'Мои экскурсии',
];
