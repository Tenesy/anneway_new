@extends('welcome')

@section('content')
<section id="subscribe-block" class="pt-50 pb-50 pt-md-100 pb-md-100 text-center light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
                <div class="content-box padding-x3 bg-default shadow" style="">
                    <h3 class="mb-20">{{ __('Login') }}</h3>
                    <form method="POST" action="/login/role" class="contact_form form-vertical mb-30" id="subscribe-block-form" novalidate="novalidate">
                        <div class="form-group text-field-group"><input required type="email" class="form-control" placeholder="Email-адрес" name="email" size="10" required></div>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <div class="form-group email-field-group"><input required type="password" class="form-control" placeholder="Пароль" name="password" size="10" required></div>
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @csrf

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-primary mt-30"><strong>Войти</strong></button>
                    </form>
                    @if (Route::has('password.request'))
                        <p class="small mb-0"><a href="{{ route('password.request') }}">Забыли пароль? Нажмите сюда, чтобы восстановить.</a></p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>

@endsection
