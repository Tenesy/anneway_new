
@extends('welcome')

@section('content')
<section id="subscribe-block" class="pt-50 pb-50 pt-md-100 pb-md-100 text-center light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
                <div class="content-box padding-x3 bg-default shadow" style="box-shadow: 0px 8px 20px 0px rgb(0 0 0 / 20%);">
                    <h3 class="mb-20">{{__('index.reset_email')}}</h3>
                    <form  method="POST" action="/reset-password" class="contact_form form-vertical mb-30" id="subscribe-block-form" novalidate="novalidate">
                        @csrf
                        @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        <div class="form-group text-field-group"><input required type="email" class="form-control" placeholder="Email-адрес*" name="email" size="10" required></div>
                        <button type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-primary mt-30"><strong>Восстановить</strong></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
@endsection
