@extends('welcome')

@section('content')
<section id="subscribe-block render" class="pt-50 pb-50 pt-md-100 pb-md-100 text-center light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
                <div class="content-box padding-x3 bg-default shadow"
                    style="box-shadow: 0px 8px 20px 0px rgb(0 0 0 / 20%);">
                    <h3 class="mb-20">Регистрация</h3>
                    <form method="POST" action="{{ route('register') }}" class="contact_form form-vertical mb-30" id="subscribe-block-form"
                        novalidate="novalidate">
                        @csrf
                        <div class="form-group text-field-group">
                            <input required type="text" class="form-control"
                                placeholder="Фамилия*" name="last_name" size="10" required>

                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                        </div>

                        <div class="form-group text-field-group">
                            <input required type="text" class="form-control"
                                placeholder="Имя*" name="name" size="10" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                        </div>


                        <div class="form-group text-field-group">
                            <input required type="text" class="form-control @error('parent_name') is-invalid @enderror"
                                placeholder="Отчество*" name="parent_name" autofocus size="10" required>
                                @error('parent_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>


                        <div class="form-group text-field-group">
                            <input required type="email" class="form-control"
                                placeholder="Email-адрес*" name="email" size="10" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="form-group email-field-group">
                            <input required type="password" class="form-control"
                                placeholder="Пароль*" name="password" size="10" required>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                        <div class="form-group email-field-group"><input required type="password" class="form-control"
                                placeholder="Пароль еще раз*" name="password_confirmation" size="10" required></div>

                        <div class="form-group select-group"><select class="form-control" name="role">
                                <option value="traveller" selected="" >Путешественник</option>
                                <option value="guide">Гид</option>
                            </select><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16"
                                width="16" class="icon svg-secondary">
                                <path
                                    d="m8 8.41421356-2.82842712-2.82842712-1.41421357 1.41421356 4.24264069 4.2426407 4.2426407-4.2426407-1.4142136-1.41421356z"
                                    fill-rule="evenodd"></path>
                            </svg></div>
                        <!-- Кнопка -->
                        <button type="submit" data-loading-text="•••" data-complete-text="Completed!"
                            data-reset-text="Try again later..."
                            class="btn btn-primary mt-30 send"><strong>Регистрация</strong></button>
                    </form>
                    <p class="small mb-0"><a href="{{route('login')}}">Уже зарегистрированы? Нажмите сюда, чтобы войти.</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
@endsection
