@extends('welcome')

@section('content')
<main>
    <section id="inner-post-sidebar" class="pt-30 pt-md-70 pb-md-30 light">
        <div class="container">
            <h4 class="mb-4 mt-1"><strong>Настройка профиля</strong></h4>
        </div>
        <div class="container">
            <div class="d-flex mb-4 card p-4">
                <!-- Первая секция -->
                <form method="post" action="/update_user" enctype="multipart/form-data">
                    <section>
                        <div class="h5 pb-2 mb-4" style="border-bottom: 1px solid lightgrey;"><b>Персональная информация</b></div>
                        <div class="row no-gutters">
                            <div class="col-md-2 col-sm-12">
                                <div class="chat-avatar">
                                    @if ($user->photo_url)
                                        <img src="{{$user->photo_url}}" alt="..." class="w-100">
                                    @else
                                        <img src="https://via.placeholder.com/168x168" alt="" class="w-100">
                                    @endif

                                </div>
                                <div class="mt-2 d-flex justify-content-center">
                                    <label for="loadGasse" class="btn btn-primary w-md waves-effect waves-light">
                                        Выберите фото
                                    </label>
                                    <input type="file" name="photo" id="loadGasse" accept="image/png, image/jpeg" style="display: none;">
                                </div>
                            </div>

                            <div class="col-md-10 col-sm-12 row	px-4">
                                <div class="col-sm-12 col-md-12">
                                    <label for="last_name" class="font-weight-normal" style="width: 100%;">
                                        Введите Фамилию*:
                                    </label>
                                    <input type="text" value="{{$user->last_name}}" name="last_name" id="last_name" class="form-control chat-input" required placeholder="Введите Фамилию">
                                </div>

                                <div class="col-sm-12 col-md-6">
                                    <label for="first_name" class="font-weight-normal" style="width: 100%;">
                                        Введите Имя*:
                                    </label>
                                    <input type="text" value="{{$user->first_name}}" required name="first_name" id="first_name" class="form-control chat-input" placeholder="Введите Имя">
                                </div>

                                <div class="col-sm-12 col-md-6">
                                    <label for="parent_name" class="font-weight-normal" style="width: 100%;">
                                        Введите Отчество:
                                    </label>
                                    <input type="text" value="{{$user->parent_name}}" name="parent_name" id="parent_name" class="form-control chat-input" placeholder="Введите Отчество">
                                </div>

                                <div class="d-flex col-md-12 justify-content-end w-md waves-effect waves-light">
                                    <input type="submit" value="Cохранить" class="btn btn-primary" style="height: 42px;">
                                </div>
                            </div>
                        </div>
                    </section>
                        <div class="row col-md-12">
                            @if ($errors->any())
                                <div class="justify-content-center alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        @csrf
                </form>
                <!-- Вторая секция -->
                <form class="mt-4" action="/contacts" method="post">
                    @csrf
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Контактные данные</b></div>
                        <div class="row	">
                            <div class="col-sm-12 col-md-12">
                                <label for="email" class="font-weight-normal" style="width: 100%;">
                                    Введите ваш Email-адрес:
                                    <div class="badge badge-success ml-1">Подтверждён</div>
                                </label>
                                <input type="text" required value="{{$user->email}}" name="email" id="email" class="form-control chat-input" placeholder="Введите Email">
                            </div>

                            <!-- Тут внимательно, т.к. тут ДИВ -->
                            <div class="col-sm-12 col-md-12 mt-2">
                                @if(count($user->telephones) && $user->telephones[0]->status)
                                    <label for="last_name" class="font-weight-normal" style="width: 100%;">
                                        Ваш Номер телефона:
                                        <div class="badge badge-success ml-1">Подтверждён</div>
                                    </label>
                                    <div class="chat-input p-2">{{$user->telephones[0]->phone}}</div>
                                @else

                                @endif
                            </div>

                            <!-- А тут инпут, не перепутай -->
                            @if(count($user->telephones) && !$user->telephones[0]->status)
                                <div class="col-sm-12 col-md-12 mt-2">
                                    <p>Это нужно использовать, если поле не подтверждено и требуется код</p>
                                    <label for="phone" class="font-weight-normal" style="width: 100%;">
                                        Введите Номер телефона:
                                        <div class="badge badge-danger ml-1">Не подтверждён</div>
                                    </label>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-8">
                                            <input type="text" name="phone" value="{{$user->telephones[0]->phone}}" id="phone" class="form-control chat-input" placeholder="Введите Номер телефона">
                                        </div>
                                        <div class="col-sm-12 col-md-4" >
                                            <input type="text" name="code"  class="form-control chat-input" placeholder="Код подтверждения">
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex col-md-4 justify-content-end w-md waves-effect waves-light mt-3">
                                    <input type="button" id="confirm" value="Подтвердить" class="btn btn-primary" style="height: 42px;">
                                </div>
                            @elseif(!count($user->telephones))
                            <div class="col-sm-12 col-md-12 mt-2">
                                <p>Это нужно использовать, если поле не подтверждено и требуется код</p>
                                <label for="phone" class="font-weight-normal" style="width: 100%;">
                                    Введите Номер телефона:
                                    <div class="badge badge-danger ml-1">Не подтверждён</div>
                                </label>
                                <div class="row">
                                    <div class="col-sm-12 col-md-8">
                                        <input type="text" name="phone" id="phone" class="form-control chat-input" placeholder="Введите Номер телефона">
                                    </div>
                                    <div class="col-sm-12 col-md-4" id="cod">
                                        <input type="text" name="code"  class="form-control chat-input" placeholder="Код подтверждения">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex col-md-4 justify-content-end w-md waves-effect waves-light mt-3">
                                <input type="button" id="confirm" value="Подтвердить" class="btn btn-primary" style="height: 42px;">
                            </div>
                            @endif
                            <div class="d-flex @if(count($user->telephones) && $user->telephones[0]->status) col-md-12 @else col-md-8 @endif justify-content-end w-md waves-effect waves-light mt-3">
                                <input type="submit" value="Cохранить" class="btn btn-primary" style="height: 42px;">
                            </div>

                        </div>

                    </section>
                </form>

                <!-- Уведомления -->
                @if (count($notifications))
                <form action="/notifications" method="post">
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Уведомления</b></div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Тип уведомления</th>
                                <th scope="col" class="text-center">Email</th>
                                <th scope="col" class="text-center">Telegram</th>
                                <th scope="col" class="text-center">SMS</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($notifications as $notification)
                                <?php
                                        if ($notifi = $user->notifications()->wherePivot('notification_id', $notification->id)->first()) {
                                            $notifi = $notifi->pivot;

                                        } else {
                                            $notifi = null;
                                        }
                                    ?>
                                    <tr>
                                        <th class="font-weight-normal">{{$notification->name}}</th>
                                        <td class="text-center">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="notificatio[{{$notification->id}}][email]" class="custom-control-input" @if ($notifi ? $notifi->email : null) checked @endif value="1" name="notificatio[{{$notification->id}}][email]">
                                                <label class="custom-control-label" for="notificatio[{{$notification->id}}][email]"></label>
                                            </div>
                                        </td>
                                        <td class="text-center"><input type="checkbox" >
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="notificatio[{{$notification->id}}][telegram]" class="custom-control-input" @if ($notifi ? $notifi->telegram : null) checked @endif value="1" name="notificatio[{{$notification->id}}][telegram]">
                                                <label class="custom-control-label" for="notificatio[{{$notification->id}}][telegram]"></label>
                                            </div>
                                        </td>
                                        <td class="text-center"><input type="checkbox" >
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" id="notificatio[{{$notification->id}}][sms]" class="custom-control-input" @if ($notifi ? $notifi->sms : null) checked @endif value="1" name="notificatio[{{$notification->id}}][sms]">
                                                <label class="custom-control-label" for="notificatio[{{$notification->id}}][sms]"></label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="d-flex col-md-12 justify-content-end w-md waves-effect waves-light mt-3">
                            <input type="submit" value="Cохранить" class="btn btn-primary" style="height: 42px;">
                        </div>
                    </section>
                    @csrf
                </form>
                @endif


                <form class="mt-4" action="/description_profile" method="post">
                <!-- Описание профиля -->
                <section>
                    <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Описание профиля</b></div>

                    <div class="col-sm-12 col-md-12 no-gutters">
                        <label for="small_desc" class="font-weight-normal" style="width: 100%;">
                            Краткое описание профиля:
                            <div class="badge badge-info ml-1">0/250</div>
                        </label>
                        <textarea name="small_desc" id="small_desc" class="form-control chat-input" rows="4" style="resize: none;">@if ($user->descriptions){{$user->descriptions->hort_description}} @endif</textarea>
                    </div>
                    <div class="col-sm-12 col-md-12 no-gutters mt-4">
                        <label for="description" class="font-weight-normal" style="width: 100%;">
                            Полное описание профиля:
                            <div class="badge badge-info ml-1">0/1500</div>
                        </label>
                        <textarea name="description" id="description" class="form-control chat-input" rows="12" style="resize: none;">@if($user->descriptions){{$user->descriptions->description}}@endif</textarea>
                    </div>

                    <div class="d-flex col-md-12 justify-content-end w-md waves-effect waves-light mt-3 no-gutters">
                        <input type="submit" value="Cохранить" class="btn btn-primary" style="height: 42px;">
                    </div>
                </section>
                @csrf
                </form>
                <!-- Смена пароля -->
                <form class="mt-4" method="post" action="/reset_password">
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Изменения пароля</b></div>

                        <div class="row">
                            <div class="col-sm-12 col-md-6 input-group">
                                <label for="password" class="font-weight-normal" style="width: 100%;">
                                    Старый пароль от аккаунта:
                                </label>
                                <input type="password" name="password" id="password" class="form-control chat-input" placeholder="Введите старый пароль">
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <label for="new_password" class="font-weight-normal" style="width: 100%;">
                                    Новый пароль от аккаунта:
                                </label>
                                <input type="password" name="new_password" id="new_password" class="form-control chat-input" placeholder="Введите желаемый пароль от акканту">
                            </div>
                        </div>
                        @csrf
                        <div class="custom-control custom-checkbox mt-3">
                            <input type="checkbox" class="custom-control-input" value="1" name="dv"  @if ($user->two_auth) checked @endif id="dv">
                            <label class="custom-control-label" for="dv">Двухфакторная аутентификация (Email)</label>
                        </div>
                        <div class="d-flex col-md-12 justify-content-end w-md waves-effect waves-light mt-3 no-gutters">
                            <input type="submit" value="Cохранить" class="btn btn-primary" style="height: 42px;">
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </section>
</main>

@endsection
