@extends('welcome')
@section('content')
<section id="inner-post-sidebar" class="pt-50 pt-md-100 pb-md-50 light">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <h3>Список заявок</h3>
            </div>
            <!-- Список зщаявок -->
            <table class="table table-hover col-md-12 col-lg-12">
                <thead class="pb-4">
                  <tr style="border-bottom: 1px solid lightgrey;">
                    <th scope="col">Клиент</th>
                    <th scope="col">Экскурсия</th>
                    <th scope="col">Когда</th>
                    <th scope="col">Количество человек</th>
                    <th scope="col">Стоимость</th>
                    <th scope="col">Статуc</th>
                    <th scope="col">Действия</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                  <tr class="table-active">
                    <td><a href="#">Иванов Иван Иванович</a></td>
                    <td><a href="#">Экурсия номер 1</a></td>
                    <td>29.09.2021 в 16:40<br/><small>Пятница</small></td>
                    <td>3 чел.</td>
                    <td>€ 410</td>
                    <td><div class="status status-wait">В ожидании</div></td>
                    <td>
                        <span class="btn btn-danger status">Удалить</span>
                    </td>
                  </tr>
                </tbody>
              </table>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
@endsection
