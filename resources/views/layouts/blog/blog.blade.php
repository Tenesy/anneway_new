@extends('welcome')
@section('content')
<header id="header-search-2" class="pt-75 pb-75 pt-md-80 pb-md-80 dark">
    <div class="container">
        <a href="/create-article" class="btn btn-primary">создать статью</a>
        <div class="row">
            <div class="col-md-7">
                <h3><strong>Блог пользователей</strong></h3>
                <p class="text-secondary mb-30">Записки о путешествии от всех пользователей!</p>
            </div>
            <div class="col-md-5">
                <form action="scripts/request.php" class="contact_form form-inline" id="header-search-2-form"
                    novalidate="novalidate">
                    <div class="form-group email-field-group"><input type="text" class="form-control"
                            placeholder="Поиск по блогу" name="SEARCH" size="10"></div>
                    <button type="submit" data-loading-text="•••" data-complete-text="Заверено!"
                        data-reset-text="Повторите позже..." class="btn btn-link"><svg
                            xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16" width="16"
                            class="icon svg-default icon-pos-left">
                            <path
                                d="m12.4199285 10.8391071 3.2713969 3.2713969c.4115661.4115661.4115661 1.0788466 0 1.4904127s-1.0788466.4115661-1.4904127 0l-3.2573956-3.2573956c-1.14199306.8523301-2.55871405 1.3569312-4.09329095 1.3569312-3.78327543 0-6.85022615-3.0669507-6.85022615-6.85022615 0-3.78327543 3.06695072-6.85022615 6.85022615-6.85022615 3.78327545 0 6.85022615 3.06695072 6.85022615 6.85022615 0 1.48814524-.4745276 2.86545861-1.2805238 3.98888095zm-5.56970235.7535833c2.61919069 0 4.74246425-2.12327356 4.74246425-4.74246425 0-2.61919068-2.12327356-4.74246426-4.74246425-4.74246426-2.61919068 0-4.74246426 2.12327358-4.74246426 4.74246426 0 2.61919069 2.12327358 4.74246425 4.74246426 4.74246425z">
                            </path>
                        </svg><strong>Найти</strong></button>
                </form>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</header>
<section id="blog-2col-sidebar" class="pt-50 pt-md-100 pb-md-50 light">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-9">
                <div class="row">
                    @foreach ($articles as $article)

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="gallery-item gallery-style-1 padding-x2 mb-50 border">
                                <a href="#"><img class="item-img" src="{{$article->image}}" alt="image"></a>
                                <div class="item-title">
                                    <h4><strong>{{$article->title}}</strong></h4>
                                    <p class="text-secondary small">{{$article->created_at->format('d-M-Y')}} | <a href="#">{{$article->category->name}}</a></p>
                                    <p class="">{{$article->description}}</p>
                                </div>
                                <div class="item-icon">
                                    <ul class="list-inline small">
                                        <li><svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 16 16" width="16" class="icon svg-secondary icon-pos-left"><path d="m14 10.7994646v5.2005354l-4.64026195-3.093508c-.44192631.0614803-.89622881.093508-1.35973805.093508-4.418278 0-8-2.9101491-8-6.5 0-3.58985087 3.581722-6.5 8-6.5s8 2.91014913 8 6.5c0 1.64847552-.7552732 3.15362319-2 4.2994646z" fill-rule="evenodd"></path></svg><a href="#"><strong>26</strong></a></li>
                                        <li><svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 16 16" width="16" class="icon svg-secondary icon-pos-left"><path d="m14.3206469 9.25651754-6.12825874 6.12825876-6.12825877-6.12825876c-1.69227194-1.69227193-1.69227194-4.43598684 0-6.12825877 1.69227193-1.69227193 4.43598683-1.69227193 6.12825877 0 1.69227193-1.69227193 4.43598684-1.69227193 6.12825874 0 1.692272 1.69227193 1.692272 4.43598684 0 6.12825877z"></path></svg><a href="#"><strong>14</strong></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{ $articles->links() }}
                </div>
            </div>
            <aside class="col-md-4 col-lg-3">
                <form action="scripts/request.php" class="contact_form form-inline mb-50" id="blog-2col-sidebar-form" novalidate="novalidate">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Поиск" name="SEARCH" size="10"></div>
                    <button type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-link"><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16" width="16" class="icon svg-default"><path d="m12.4199285 10.8391071 3.2713969 3.2713969c.4115661.4115661.4115661 1.0788466 0 1.4904127s-1.0788466.4115661-1.4904127 0l-3.2573956-3.2573956c-1.14199306.8523301-2.55871405 1.3569312-4.09329095 1.3569312-3.78327543 0-6.85022615-3.0669507-6.85022615-6.85022615 0-3.78327543 3.06695072-6.85022615 6.85022615-6.85022615 3.78327545 0 6.85022615 3.06695072 6.85022615 6.85022615 0 1.48814524-.4745276 2.86545861-1.2805238 3.98888095zm-5.56970235.7535833c2.61919069 0 4.74246425-2.12327356 4.74246425-4.74246425 0-2.61919068-2.12327356-4.74246426-4.74246425-4.74246426-2.61919068 0-4.74246426 2.12327358-4.74246426 4.74246426 0 2.61919069 2.12327358 4.74246425 4.74246426 4.74246425z"></path></svg></button>
                </form>

                @include('layouts.blog.block.category')

                <div class="content-box mb-50">
                    <h4 class="mb-20"><strong>Облако тэгов</strong></h4>
                    <div class="btn-cloud">
                        <a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a>
                    </div>
                </div>
                <div class="content-box mb-50">
                    <h4><strong>Архив блога</strong></h4>
                    <ul class="list-unstyled padding-x2-list separate-list small">
                        <li><a href="#">January</a> <span class="float-right">14</span></li>
                        <li><a href="#">February</a> <span class="float-right">14</span></li>
                        <li><a href="#">March</a> <span class="float-right">14</span></li>
                        <li><a href="#">April</a> <span class="float-right">14</span></li>
                        <li><a href="#">May</a><span class="float-right">14</span></li>
                        <li><a href="#">June</a><span class="float-right">14</span></li>
                        <li><a href="#">July</a><span class="float-right">14</span></li>
                        <li><a href="#">August</a><span class="float-right">14</span></li>
                        <li><a href="#">September</a><span class="float-right">14</span></li>
                        <li><a href="#">October</a><span class="float-right">14</span></li>
                        <li><a href="#">November</a><span class="float-right">14</span></li>
                        <li><a href="#">December</a><span class="float-right">14</span></li>
                    </ul>
                </div>

                <a href="#"><img class="mw-100 mb-50" src="https://via.placeholder.com/320x100" srcset="images/banner-336280@2x.jpg 2x" alt="banner"></a>

            </aside>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
@endsection
