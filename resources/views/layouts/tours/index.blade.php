@extends('welcome')

@section('content')
<form action="{{route('tours.store')}}" method="post" enctype="multipart/form-data">
    {{csrf_field()}}
    <main>
        <section id="inner-post-sidebar" class="pt-30 pt-md-70 pb-md-30 light">
            <div class="container">
                <h4 class="mb-4 mt-1"><strong>Добавление экскурсии</strong></h4>
                <p>Расскажите о своей экскурсии. Опишите как можно детальнее вашу экскурсию. Если у вас будут вопросы по поводу оформления —  <a href="Ссылка на чат с тех.поддержкой">напишите нам</a>!
                </p>
            </div>
            <div class="container">
                <div class="d-flex mb-4 card p-4">
                    <!-- Описание профиля -->
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Основная информация</b></div>

                        <div class="col-sm-12 col-md-12 no-gutters">
                            <label for="title" class="font-weight-normal" style="width: 100%;">
                                <b>Название экскурсии:</b> <div class="badge badge-info ml-1">0/140</div>
                            </label>
                            <input type="text" name="title" id="title" class="form-control chat-input" placeholder="Введите название экскурсии">
                        </div>
                        <div class="col-sm-12 col-md-12 no-gutters mt-4">
                            <label for="description" class="font-weight-normal" style="width: 100%;">
                                <b>Информация об экскурсии:</b>
                                <p>Представьте, что экскурсии на эту тему проводят разные гиды.
                                    Чтобы убедить людей пойти на экскурсию с вами, нужно показать, чем вы отличаетесь от других гидов.
                                </p>
                                <p>
                                    <b>Опишите:</b> В чем заключается ваш подход?
                                    Что именно вы хотите рассказать на экскурсии? Какие места вы выбрали и почему?
                                    И самое главное, напишите, что лично вас вдохновляет в теме экскурсии. Вдохновите этим других людей!
                                </p>
                                <div class="badge badge-info ml-1">0/1500</div>
                            </label>
                            <textarea name="description" id="description" class="form-control chat-input" rows="8" style="resize: none;"></textarea>
                        </div>

                        <div class="col-sm-12 col-md-12 no-gutters mt-4">
                            <label for="details" class="font-weight-normal" style="width: 100%;">
                                <b>Организационные детали:</b>
                                <p>Напишите, о чем стоит знать путешественникам перед экскурсией.
                                    Например, о дополнительных расходах, деталях трансфера, о том, что взять с собой, и других важных моментах.
                                    Либо укажите, что таких важных моментов нет.
                                </p>
                                <div class="badge badge-info ml-1">0/800</div>
                            </label>
                            <textarea name="details" id="details" class="form-control chat-input" rows="8" style="resize: none;"></textarea>
                        </div>

                        {{-- <div class="d-flex col-md-12 justify-content-end w-md waves-effect waves-light mt-3 no-gutters">
                            <input type="submit" value="Cохранить" class="btn btn-primary" style="height: 42px;">
                        </div> --}}
                    </section>
                    <!-- Условия -->
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Информация об условиях проведения</b></div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <label for="city" class="font-weight-normal" style="width: 100%;">
                                    <b>Выберите город:</b>
                                </label>
                                <select name="city" id="city" class="p-2 w-100 chat-input">
                                    <option value="1">Рим, Титалия</option>
                                    <option value="2">Анталия, Турция</option>
                                    <option value="3">Барселона, Испания</option>
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-12 mt-2">
                                <label for="language" class="font-weight-normal" style="width: 100%;">
                                    <b>Язык экскурсии:</b>
                                    <p>Выберите языки, на которых вы можете провести экскурсию (не более 3 языков).</p>
                                </label>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <p class="p-2 chat-input">Немецкий</p>
                                    </div>
                                    <div class="col-sm-2">
                                        <select name="language" id="language" class="p-2 w-100 chat-input">
                                            <option value="1">Русский</option>
                                            <option value="2">Украинский</option>
                                            <option value="3">Английский</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 mt-2">
                                <label for="duration" class="font-weight-normal" style="width: 100%;">
                                    <b>Продолжительность экскурсии:</b>
                                </label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <select type="password" name="duration" id="duration" class="p-2 w-100 chat-input" placeholder="Введите старый пароль">
                                            <option value="0.5">0.5 часа</option>
                                            <option value="1">1 час</option>
                                            <option value="1.5">1.5 часа</option>
                                            <option value="2">2 часа</option>
                                            <option value="2.5">2.5 часа</option>
                                            <option value="3">3 часа</option>
                                            <option value="3.5">3.5 часа</option>
                                            <option value="4">4 часа</option>
                                            <option value="4.5">4.5 часа</option>
                                            <option value="5">5 часов</option>
                                            <option value="5.5">5.5 часа</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 mt-2">
                                <label for="count_user" class="font-weight-normal" style="width: 100%;">
                                    <b>Максимальное количество участников:</b>
                                </label>
                                <div class="row">
                                    <!-- <div class="col-sm-2">
                                        <p class="p-2 chat-input">Немецкий</p>
                                    </div> -->
                                    <div class="col-sm-4">
                                        <input type="text" name="count_user" id="count_user" class="form-control chat-input" placeholder="">
                                    </div>
                                    <div class="col-sm-8 d-flex align-items-center">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="children" class="custom-control-input" value="1" id="children">
                                            <label class="custom-control-label" for="children">Можно с детьми</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 mt-2">
                                <label for="transport" class="font-weight-normal" style="width: 100%;">
                                    <b>Способ передвижения на экскурсии:</b>
                                </label>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <select name="transport" id="transport" class="p-2 w-100 chat-input">
                                            <option value="1">Выберите...</option>
                                            <option value="Пешком">Пешком</option>
                                            <option value="На машине">На машине</option>
                                            <option value="На велосипеде">На велосипеде</option>
                                            <option value="На мотоцикле">На мотоцикле</option>
                                            <option value="На водном судне">На водном судне</option>
                                            <option value="На автобусе">На автобусе</option>
                                            <option value="В помещении">В помещении</option>
                                            <option value="Другой тип передвижения">Другой тип передвижения</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Расписание и время -->
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Расписание и время экскурсии</b></div>
                            <b>Выберите тип экскурсии:</b>
                            <p>От типа экскурсии будет зависеть формирование вашего расписания и календаря. Выбирайте внимательно!</p>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="type_tours" name="type_tours" value="personal" class="custom-control-input">
                                        <label class="custom-control-label" for="type_tours">Персональная экскурсия</label>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-4">
                                    <b>Распиание и время начала:</b>
                                    <p>Создайте распиание на неделе, когда вы проводите экскурсию</p>
                                </div>
                                <!-- Дни недели -->
                                <div class="col-sm-12 mt-2 row">
                                    <div class="col-md-3 d-flex align-items-center">
                                        Понеденик:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="monday_start" id="monday_start" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-center">
                                        до:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="monday_end" id="monday_end" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-2 row">
                                    <div class="col-md-3 d-flex align-items-center">
                                        Вторник:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tuesday_start" id="tuesday_start" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-center">
                                        до:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tuesday_end" id="tuesday_end" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-2 row">
                                    <div class="col-md-3 d-flex align-items-center">
                                        Среда:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="wednesday_start" id="wednesday_start" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-center">
                                        до:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="wednesday_end" id="wednesday_end" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-2 row">
                                    <div class="col-md-3 d-flex align-items-center">
                                        Четверг:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="thursday_start" id="thursday_start" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-center">
                                        до:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="thursday_end" id="thursday_end" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-2 row">
                                    <div class="col-md-3 d-flex align-items-center">
                                        Пятница:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="friday_start" id="friday_start" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-center">
                                        до:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="friday_end" id="friday_end" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-2 row">
                                    <div class="col-md-3 d-flex align-items-center">
                                        Суббота:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tml_select" id="tml_select" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-center">
                                        до:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tml_select" id="tml_select" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 mt-2 row">
                                    <div class="col-md-3 d-flex align-items-center">
                                        Воскресенье:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tml_select" id="tml_select" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                    <div class="col-md-1 d-flex align-items-center">
                                        до:
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tml_select" id="tml_select" class="p-2 w-100 chat-input">
                                            <option value="0">На это день нет эскурсий</option>
                                            <option value="08:00">08:00</option>
                                            <option value="09:00">09:00</option>
                                            <option value="10:00">10:00</option>
                                            <option value="11:00">11:00</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Конец дней недели -->
                                <div class="col-sm-12 col-md-6 mt-3">
                                    <label for="minimum_booking_time" class="font-weight-normal" style="width: 100%;">
                                        <b>Минимальное время время бронирования:</b>
                                    </label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <select type="password" name="minimum_booking_time" id="minimum_booking_time" class="p-2 w-100 chat-input" placeholder="Введите старый пароль">
                                                <option value="0">1 час</option>
                                                <option value="0">2 часа</option>
                                                <option value="0">3 часа</option>
                                                <option value="0">6 часов</option>
                                                <option value="0">12 часов</option>
                                                <option value="0">24 часа</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 mt-3">
                                    <label for="maximum_booking_time" class="font-weight-normal" style="width: 100%;">
                                        <b>Максимальное время бронирования:</b>
                                    </label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <select type="password" name="maximum_booking_time" id="maximum_booking_time" class="p-2 w-100 chat-input" placeholder="Введите старый пароль">
                                                <option value="0">1 месяц</option>
                                                <option value="0">2 месяца</option>
                                                <option value="0">3 месяца</option>
                                                <option value="0">4 месяца</option>
                                                <option value="0">5 месяцев</option>
                                                <option value="0">6 месяцев</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </section>
                    <!-- Место встречи -->
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Место встречи экскурсии</b></div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12 input-group">
                                <label for="place_tours" class="font-weight-normal" style="width: 100%;">
                                    <b>Примерное место встречи:</b>
                                </label>
                                <input type="text" name="place_tours" id="place_tours" class="form-control chat-input" placeholder="К примеру: Около станции метро Арсенальная">
                            </div>
                            <div class="col-sm-12 col-md-12 input-group mt-3">
                                <label for="place_city" class="font-weight-normal" style="width: 100%;">
                                    <b>Точное описание места встречи:</b>
                                    <div class="badge badge-info ml-1">0/1000</div>
                                </label>
                                <textarea name="place_city" id="place_city" class="form-control chat-input" rows="8" style="resize: none;"></textarea>
                            </div>
                        </div>
                    </section>
                    <!-- Стоимость -->
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Стоимость экскурсии</b></div>

                        <div class="row">
                            <div class="col-sm-6 col-md-6 input-group">
                                <label for="price_human1" class="font-weight-normal" style="width: 100%;">
                                    <b>Стоимость за 1 человека:</b>
                                </label>
                                <input type="text" name="price_human1" id="price_human1" class="form-control chat-input" placeholder="100$">
                            </div>
                            <div class="col-sm-6 col-md-6 input-group">
                                <label for="price_tours" class="font-weight-normal" style="width: 100%;">
                                    <b>Стоимость за всю экскурсию:</b>
                                </label>
                                <input type="text" name="price_tours" id="price_tours" class="form-control chat-input" placeholder="1400$">
                            </div>
                            <div class="col-sm-6 col-md-6 input-group mt-2">
                                <label for="places_oldd" class="font-weight-normal" style="width: 100%;">
                                    <b>Скидка?</b>
                                </label>
                                <select type="password" name="discount" id="last_name" class="p-2 w-100 chat-input" placeholder="Введите старый пароль">
                                    <option value="0">Нет скидки</option>
                                    <option value="0">Скидка 10%</option>
                                    <option value="0">Скидка 15%</option>
                                    <option value="0">Скидка 20%</option>
                                    <option value="0">Скидка 25%</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-md-6 input-group mt-2">
                                <label for="places_oldd" class="font-weight-normal" style="width: 100%;">
                                    <b>Итоговая стоимость:</b>
                                </label>
                                <span> Цена экскурсии со скидкой + 15% (Комиссия сервиса за размещение)</span>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Загрузить фотографии экскурсии</b></div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p>Загрузите фото своей экскурсии, чтобы пользователи смогли оценить и погрузиться в атмосферу экскурсии. Изображение должно быть горизонтальным, и в хорошем качестве.</p>
                                {{-- <div class="row col-sm-12">
                                    <div class="chat-avatar mr-2">
                                        <img src="https://via.placeholder.com/168x168" alt="" style="max-width: 240px;">
                                    </div>
                                    <div class="chat-avatar mr-2">
                                        <img src="https://via.placeholder.com/168x168" alt="" style="max-width: 240px;">
                                    </div>
                                    <div class="chat-avatar mr-2">
                                        <img src="https://via.placeholder.com/168x168" alt="" style="max-width: 240px;">
                                    </div>
                                    <div class="chat-avatar mr-2">
                                        <img src="https://via.placeholder.com/168x168" alt="" style="max-width: 240px;">
                                    </div>
                                </div> --}}
                            </div>
                            <div class="col-sm-12 mt-3 d-flex justify-content-center">
                                <label for="photo" class="btn btn-primary d-flex justify-content-center" style="max-width: 320px;">
                                    Выберите файлы для загрузки <i class="ri-upload-line ml-2"></i>
                                </label>
                                <input type="file" name="photo" id="photo" style="display: none;">
                            </div>
                        </div>
                    </section>
                    <!-- Смена пароля -->
                    <section>
                        <div class="h5 pb-2 my-4" style="border-bottom: 1px solid lightgrey;"><b>Сохраните экскурсию</b></div>
                        <p>Проверьте данные экскурсии и фото и нажмите эту кнопку, чтобы отпарвить экскурсию на модерацию</p>
                        <div class="d-flex col-md-12 justify-content-center w-md waves-effect waves-light mt-3 no-gutters">
                            <input type="submit" value="Отправить экскурсию на модерацию" class="btn btn-primary" style="height: 42px;">
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </main>
</form>
@endsection

