@extends('welcome')
@section('content')
<header id="header-search-2" class="pt-75 pb-75 pt-md-80 pb-md-80 dark">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h3><strong>Все экскурсии</strong></h3>
                {{-- <p class="text-secondary mb-30">Экскурсии в городе {ГОРОД} от 25$!</p> --}}
            </div>
            <div class="col-md-5">
                <form action="scripts/request.php" class="contact_form form-inline" id="header-search-2-form"
                    novalidate="novalidate">
                    <div class="form-group email-field-group"><input type="text" class="form-control"
                            placeholder="Поиск по экскурсиям" name="SEARCH" size="10"></div>
                    <button type="submit" data-loading-text="•••" data-complete-text="Заверено!"
                        data-reset-text="Повторите позже..." class="btn btn-link"><svg
                            xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16" width="16"
                            class="icon svg-default icon-pos-left">
                            <path
                                d="m12.4199285 10.8391071 3.2713969 3.2713969c.4115661.4115661.4115661 1.0788466 0 1.4904127s-1.0788466.4115661-1.4904127 0l-3.2573956-3.2573956c-1.14199306.8523301-2.55871405 1.3569312-4.09329095 1.3569312-3.78327543 0-6.85022615-3.0669507-6.85022615-6.85022615 0-3.78327543 3.06695072-6.85022615 6.85022615-6.85022615 3.78327545 0 6.85022615 3.06695072 6.85022615 6.85022615 0 1.48814524-.4745276 2.86545861-1.2805238 3.98888095zm-5.56970235.7535833c2.61919069 0 4.74246425-2.12327356 4.74246425-4.74246425 0-2.61919068-2.12327356-4.74246426-4.74246425-4.74246426-2.61919068 0-4.74246426 2.12327358-4.74246426 4.74246426 0 2.61919069 2.12327358 4.74246425 4.74246426 4.74246425z">
                            </path>
                        </svg><strong>Найти</strong></button>
                </form>
            </div>
        </div>
        <div class="row no-gutters">
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Все <span>{{count($tours)}}</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Есть скидка <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Обзор города <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">История/Архитектура <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Для детей <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Гастрономический <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">За городом <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Музеи и искусство <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Фотосессии <span>0</span></a>
            <a href="#" class="badge my-2 badge-light mr-2 search-tag">Необычные маршруты <span>0</span></a>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</header>
<section id="contact-inline-form" class="py-4 light">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3><strong>Фильтр</strong></h3>
                <form action="./scripts/request.php"
                    class="contact_form form-inline mb-50 shadow bg-default padding-x2"
                    id="contact-inline-form-form" novalidate="novalidate">
                    <div class="form-group text-field-group"><input type="text" class="form-control"
                            placeholder="Сколько человек?" name="NAME" size="10"></div>
                    <div class="form-group email-field-group"><input type="email" class="form-control"
                            placeholder="Цена" name="EMAIL" size="10"></div>
                    <div class="form-group datepicker-group"><input type="text"
                            class="form-control datepicker-input" placeholder="Выберите дату" name="DATE"
                            size="10"><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16"
                            width="16" class="icon svg-secondary">
                            <path
                                d="m0 .79344249c0-.43820619.36424446-.79344249.79344249-.79344249h14.41311501c.4382062 0 .7934425.36424446.7934425.79344249v14.41311501c0 .4382062-.3642445.7934425-.7934425.7934425h-14.41311501c-.43820619 0-.79344249-.3642445-.79344249-.7934425zm2 13.20655751h12v-9h-12zm2-5h2v-2h-2zm0 3h2v-2h-2zm3 0h2v-2h-2zm3 0h2v-2h-2zm-3-3h2v-2h-2zm3 0h2v-2h-2z">
                            </path>
                        </svg></div>
                    <div class="form-group select-group"><select class="form-control" name="TYPE">
                            <option value="Choose type" selected="" disabled="">Способ передвижения</option>
                            <option value="Type 1">Type 1</option>
                            <option value="Type 2">Type 2</option>
                            <option value="Type 3">Type 3</option>
                        </select><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16"
                            width="16" class="icon svg-secondary">
                            <path
                                d="m8 8.41421356-2.82842712-2.82842712-1.41421357 1.41421356 4.24264069 4.2426407 4.2426407-4.2426407-1.4142136-1.41421356z"
                                fill-rule="evenodd"></path>
                        </svg></div>
                    <button type="submit" data-loading-text="•••" data-complete-text="Completed!"
                        data-reset-text="Try again later..."
                        class="btn btn-link"><strong>Применить</strong></button>
                </form>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
<section id="blog-3col" class="pt-75 pb-75 pt-lg-100 pb-lg-100 light">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3><strong>Список всех экскурсий</strong></h3>
                <svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 40 20" width="40"
                    class="mb-50 svg-secondary">
                    <path d="m0 8h40v4h-40z" fill-rule="evenodd"></path>
                </svg>
            </div>

            @foreach ($tours as $tour)
            {{-- {{dd($tour->photos)}} --}}
            <div class="col-lg-4">
                <div class="gallery-item gallery-style-1 padding-x2 mb-50" style="background-color: #f4f5f6;">
                    @if (count($tour->photos))
                        <a href="/info-tours/{{$tour->id}}"><img class="item-img" src="{{$tour->photos[0]->photo}}" alt="image"></a>
                    @else
                        <a href="/info-tours/{{$tour->id}}"><img class="item-img" src="https://via.placeholder.com/400x280" alt="image"></a>
                    @endif
                    <div class="item-title">
                        <h4><strong>{{$tour->title}}</strong></h4>
                        <p class="text-secondary mb-0 small">{{$tour->created_at->format('d-M-Y')}} | <a href="#">{{$tour->user->first_name. " ".$tour->user->last_name}}</a></p>
                    </div>
                    <div class="item-icon">
                        <ul class="list-inline small">
                            <li><svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 16 16"
                                    width="16" class="icon svg-secondary icon-pos-left">
                                    <path
                                        d="m14 10.7994646v5.2005354l-4.64026195-3.093508c-.44192631.0614803-.89622881.093508-1.35973805.093508-4.418278 0-8-2.9101491-8-6.5 0-3.58985087 3.581722-6.5 8-6.5s8 2.91014913 8 6.5c0 1.64847552-.7552732 3.15362319-2 4.2994646z"
                                        fill-rule="evenodd"></path>
                                </svg><a href="#"><strong>26</strong></a></li>
                            <li><svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 16 16"
                                    width="16" class="icon svg-secondary icon-pos-left">
                                    <path
                                        d="m14.3206469 9.25651754-6.12825874 6.12825876-6.12825877-6.12825876c-1.69227194-1.69227193-1.69227194-4.43598684 0-6.12825877 1.69227193-1.69227193 4.43598683-1.69227193 6.12825877 0 1.69227193-1.69227193 4.43598684-1.69227193 6.12825874 0 1.692272 1.69227193 1.692272 4.43598684 0 6.12825877z">
                                    </path>
                                </svg><a href="#"><strong>14</strong></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-12 text-center">
                <a href="#" class="btn btn-secondary mt-50"><span>View more</span></a>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
@endsection
