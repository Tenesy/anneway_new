@extends('welcome')
@section('content')
<header id="header-text-6" class="pt-75 pb-75 pt-md-150 pb-md-150 dark">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md">
                <h3><strong>{{$tours->title}}</strong></h3>
                <p class="text-secondary"><a href="#">Ближайщая: {{$tours->start}}</a> | <a href="#">Гид {{$tours->first_name}}</a> | <a href="#">Категория</a></p>
            </div>
            <div class="col-md-auto">
                <a href="/all-tours" class="btn btn-outline-light"><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16" width="16" class="icon svg-default icon-pos-left"><path d="m3.82528129 7h11.17471871v2h-11.16842704l3.23949485 3.2394949-1.41421356 1.4142135-4.24264069-4.24264067-1.41421356-1.41421357 5.65685425-5.65685425 1.41421356 1.41421357z" fill-rule="evenodd"></path></svg> Список экскурсий</a>
            </div>
        </div>
    </div>
    <div class="bg-wrap" style="background: url(https://554a875a-71dc-4f5f-b6bf-ae8967f137d5.selcdn.net/thumbs2/488ddf8e-ee90-11ea-baf1-4a024718a184.870x490.jpg); background-repeat: no-repeat; background-position: center; background-size: cover; filter: brightness(0.5);">
        <div class="bg"></div>
    </div>
</header>

<main>
    <section id="inner-post-sidebar" class="pt-50 pt-md-100 pb-md-50 light">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-8">
                    <img alt="img" class="mw-100 mb-50" src="{{$tours->photos[0]->photo}}">
                    <h3>{{$tours->title}}</h3>
                    <p class="text-secondary mb-50">{{$tours->created_at}} | <a href="#">Author name</a></p>
                    <p class="lead">{{$tours->description}}</p>
                    <hr>
                    {{-- <p>We could paste here the part of some article or couple of paragraphs from interesting book (that you have read for sure and may be even liked) or text in unfamiliar language ever. But we did not. We spent time on writing our own text, the text that you are reading now just like users will read your content in nearest future. </p>
                    <p>Of course, this text is just a little thing, one of those smalls that create this product in whole. They are – each of them – are responsible for the fact whether this product would be great or ordinary. It is possible to raise the whole product up only through raising smalls on a new level. So, this is what we are busy now. We spend our time and attention exactly to these things.</p> --}}
                    <p class="lead padding-x3 mt-30 mb-30" style="background-color: #f4f5f6;"><strong>This is a great product. It reveals an individual approach to each customer.</strong></p>
                    {{-- <ul class="padding-list">
                        <li>Feature description</li>
                        <li>Feature description</li>
                        <li>Feature description</li>
                        <li>Feature description</li>
                        <li>Feature description</li>
                    </ul> --}}

                    <section id="inner-comments" class="pt-50 pb-50 light">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 ml-auto mr-auto">
                                    <h3 class="text-center text-bold">1277 отзывов путешественников</h3>
                                    <div class="row">
                                        <div class="col-md-12 d-flex align-items-center">
                                            <div class="rating mr-2 d-flex justify-content-center align-items-center">
                                                9.8
                                            </div>
                                            <div class="mr-2">Отлично &#183; 76 отзыва </div>
                                            <a href="#">Читать все отзывы</a>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="small col-md-12">
                                            Написать отзыв могут только те, кто посетили данную экскурсию. <a href="">Подробнее</a>
                                        </div>
                                    </div>
                                    <!-- Комментарий -->
                                    <div class="media">
                                        <img class="mr-20 rounded-circle" src="images/avatar-3.jpg" alt="photo" width="50px">
                                        <div class="media-body">
                                            <div class="row no-gutters d-flex justify-content-between">
                                                <div class="">
                                                    <p class="mb-0"><strong><a href="#">Иван Иванов</a></strong></p>
                                                    <p class="text-secondary small mb-10">10 april 2018 | <a href="#"><strong>Reply</strong></a></p>
                                                </div>
                                                <div class="">
                                                    <div class="rating mr-2 d-flex justify-content-center align-items-center">
                                                        10
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media-content">
                                                <p>We want the template you downloaded look unique and new for such a long time as it is possible. In our work we try to use only the most modern, convenient and interesting solutions.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Комментарий -->
                                    <div class="media">
                                        <img class="mr-20 rounded-circle" src="images/avatar-3.jpg" alt="photo" width="50px">
                                        <div class="media-body">
                                            <div class="row no-gutters d-flex justify-content-between">
                                                <div class="">
                                                    <p class="mb-0"><strong><a href="#">Иван Иванов</a></strong></p>
                                                    <p class="text-secondary small mb-10">10 april 2018 | <a href="#"><strong>Reply</strong></a></p>
                                                </div>
                                                <div class="">
                                                    <div class="rating mr-2 d-flex justify-content-center align-items-center">
                                                        10
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media-content">
                                                <p>We want the template you downloaded look unique and new for such a long time as it is possible. In our work we try to use only the most modern, convenient and interesting solutions.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Комментарий -->
                                    <div class="media">
                                        <img class="mr-20 rounded-circle" src="images/avatar-3.jpg" alt="photo" width="50px">
                                        <div class="media-body">
                                            <div class="row no-gutters d-flex justify-content-between">
                                                <div class="">
                                                    <p class="mb-0"><strong><a href="#">Иван Иванов</a></strong></p>
                                                    <p class="text-secondary small mb-10">10 april 2018 | <a href="#"><strong>Reply</strong></a></p>
                                                </div>
                                                <div class="">
                                                    <div class="rating mr-2 d-flex justify-content-center align-items-center">
                                                        10
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="media-content">
                                                <p>We want the template you downloaded look unique and new for such a long time as it is possible. In our work we try to use only the most modern, convenient and interesting solutions.</p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="bg-wrap">
                            <div class="bg"></div>
                        </div>
                    </section>

                </div>
                <aside class="col-md-5 col-lg-4">
                    <div class="content-box mb-50 padding-x2 dark" style="background-image: url(images/bg-25.jpg); background-size: cover; background-position: center center; background-repeat: no-repeat;">
                        <h4 class="mb-20 text-center"><strong>Иван Иванов</strong></h4>


                        <div class="row">
                            <div class="col-md-6"><img src="./images/avatar-3.jpg" alt="" style="border-radius: 50%; width: 140px;"></div>
                            <div class="col-md-6 d-flex align-items-center flex-wrap">
                                <div>Представитель агенства в Калининграде</div>
                                <small>Среднее время ответа ~ 30 минут</small>
                            </div>
                        </div>
                        <small></small>
                        <div class="d-flex justify-content-center mt-3">
                            <a href="#" class="btn btn-sm btn-warning">Написать сообщение</a>
                        </div>

                    </div>

                    <div class="content-box mb-50 padding-x2" style="background-image: url(images/envelope-bg.png); background-size: contain; background-position: center top; background-repeat: no-repeat; background-color: #f4f5f6;">
                        <h4><strong>Стоимость: 45$</strong></h4>
                        <p class="text-bold">
                            <strong>Комиссия:</strong> 14$<br/>
                            <strong>Рейтинг:</strong> 8.4<br/>
                            <strong>Длительность:</strong> 5 часов<br/>
                            <strong>Размер группы:</strong> 10 человек<br/>
                            <strong>Дети:</strong> можно с детьми<br/>
                            <strong>Тип экскурсии:</strong> На автобусе<br/>
                        </p>
                        <span type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-sm btn-primary mt-20 btn-block"><strong>Заказать экскурсию</strong></span>
                    </div>

                </aside>
            </div>
        </div>
    </section>
</main>
@endsection
