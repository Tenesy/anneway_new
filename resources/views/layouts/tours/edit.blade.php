@extends('welcome')

@section('content')
<form action="{{'/tours/update/'.$tours->id}}" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <section>
        <div class="row col-md-12 mt-3 mb-3">
            <h3>{{__('thours.adding_an_excursion')}}</h3>
            <p>
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Repellendus numquam sint molestias nihil non quae
                delectus magnam, fuga tempora accusamus. Accusantium asperiores consectetur, repellat mollitia corrupti
                exercitationem adipisci at velit.
            </p>
        </div>
        <hr>

        <div class="rov col-md-12 nt-3 mb-3">
            <div class="justify-content-center d-flex">
                <h2>{{__('thours.excursion_theme')}}</h2>
            </div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non suscipit odit quas iste, eius voluptatibus
                voluptas repellendus animi veniam sed quaerat sit accusantium provident debitis ea quasi vero impedit aut.
            </p>
            <input type="text" name="title" value="{{$tours->title}}" required class="form-control1">
        </div>

        <div class="rov col-md-12 nt-3 mb-3 mt-5">
            <div class="justify-content-center d-flex">
                <h2>{{__('thours.content')}}</h2>
            </div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non suscipit odit quas iste, eius voluptatibus
                voluptas repellendus animi veniam sed quaerat sit accusantium provident debitis ea quasi vero impedit aut.
            </p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime similique modi reiciendis labore error
                perferendis! Iusto magnam, rerum nihil unde vero, dolore et aliquam at vel beatae natus non perspiciatis!
            </p>

            <textarea type="text" name="description"  required class="form-control1" rows="5">{{$tours->description}}</textarea>
        </div>

        <div class="rov col-md-12 nt-3 mb-3 mt-5">
            <div class="justify-content-center d-flex">
                <h2>{{__('thours.organizational_details')}}</h2>
            </div>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non suscipit odit quas iste, eius voluptatibus
                voluptas repellendus animi veniam sed quaerat sit accusantium provident debitis ea quasi vero impedit aut.
            </p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime similique modi reiciendis labore error
                perferendis! Iusto magnam, rerum nihil unde vero, dolore et aliquam at vel beatae natus non perspiciatis!
            </p>

            <textarea type="text" name="details" required class="form-control1" rows="5">{{$tours->details}}</textarea>
        </div>

    </section>
    <section id="condition">
        <div class="rov col-md-12 nt-3 mb-3 mt-5">
            <div class="justify-content-center d-flex">
                <h2>{{__('thours.conditions')}}</h2>
            </div>
            <div class="col-md-12 d-flex">
                <div class="col-md-3">
                    <label>{{__('thours.city')}}</label>
                </div>
                <div class="col-md-9">
                    <select class="form-control1" name="city" required>
                        <option value="0" selected>Добавить</option>
                        <option @if ($tours->conditions[0]->city == 'Киев') selected @endif>Киев</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12 d-flex mt-3">
                <div class="col-md-3">
                    <label>{{__('thours.language')}}</label>
                </div>
                <div class="col-md-9">
                    <div class="d-flex">
                        <span class="mr-3 d-flex justify-content-center align-items-center">Русский <i
                                class="icon-cross ml-2 "></i></span>

                        <select class="form-control1" name="language">
                            <option value="0">Добавить</option>
                            <option @if ($tours->conditions[0]->language == 'Украинский') selected @endif  value="Украинский">Украинский</option>
                        </select>

                    </div>
                    <small>lorem ipsum dolor sit</small>

                </div>
            </div>

            <div class="col-md-12 d-flex mt-3">
                <div class="col-md-3">
                    <label>{{__('thours.duration')}}</label>
                </div>
                <select class="form-control1" name="duration">
                    <option value="0">Добавить</option>
                    <option @if ($tours->conditions[0]->duration == '3') selected @endif value="3">3 часа</option>
                </select>

            </div>

            <div class="col-md-12 d-flex mt-3">
                <div class="col-md-3">
                    <label>{{__('thours.number_of_participants')}}</label>
                </div>
                <div class="d-flex col-md-8 ">
                    <select class="form-control1 mr-4 " name="count_user">
                        <option value="0">Добавить</option>
                        <option  @if ($tours->conditions[0]->count_user == '2') selected @endif  value="2">2</option>
                    </select>

                    <input type="checkbox" @if ($tours->conditions[0]->children == '1') checked @endif value="1" name="children" class="mr-4">
                    <span>{{__('thours.possible_children')}}</span>
                </div>

            </div>

        </div>
    </section>

    <section>
        <div class="row col-md-12">
            <div class="d-flex justify-content-center">
                <h3>{{__('thours.schedule_and_start_time')}}</h3>
            </div>
            <div class="col-md-3">
                    <label>{{__('thours.excursion_type')}}</label>
            </div>
            <div class="col-md-9">
                <uL>
                    <li><input type="radio" @if ($tours->time_starts[0]->type_tours == 1) checked @endif name="type_tours" value="1"> <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos maiores recusandae voluptas velit quisquam. Cum, vel dolor aperiam ipsa laudantium necessitatibus harum cupiditate quibusdam sunt perspiciatis dolore ab assumenda? Impedit.</span></li>
                    <li><input type="radio" @if ($tours->time_starts[0]->type_tours == 2) checked @endif name="type_tours" value="2"> <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos maiores recusandae voluptas velit quisquam. Cum, vel dolor aperiam ipsa laudantium necessitatibus harum cupiditate quibusdam sunt perspiciatis dolore ab assumenda? Impedit.</span></li>
                    <li><input type="radio" @if ($tours->time_starts[0]->type_tours == 3) checked @endif name="type_tours" value="3"> <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos maiores recusandae voluptas velit quisquam. Cum, vel dolor aperiam ipsa laudantium necessitatibus harum cupiditate quibusdam sunt perspiciatis dolore ab assumenda? Impedit.</span></li>
                </uL>
            </div>
            <div class="col-md-3">
                <label>{{__('thours.schedule_and_start_time')}}</label>
            </div>
            <div class="col-md-9">
                <h4>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Error facilis</h4>
                <uL>
                    <li><i class="icon-checkmark"> </i>пн <select name="monday_start">
                        <option value="0">добавить</option>
                        <option @if ($tours->time_starts[0]->monday_start == '10') selected @endif value="10">10</option>
                    </select> - <select name="monday_end">
                        <option value="0">добавить</option>
                        <option @if ($tours->time_starts[0]->monday_end == '16') selected @endif value="16">16</option>
                    </select> <i class="icon-cross"></i>  <small>завершится до 14:00 </small></li>
                    <li>вт <a class="ml-3" href="#">добавить</a></li>
                    <li>ср <a class="ml-3" href="#">добавить</a></li>
                    <li>чт <a class="ml-3" href="#">добавить</a></li>
                    <li>пт <a class="ml-3" href="#">добавить</a></li>
                    <li>сб <a class="ml-3" href="#">добавить</a></li>
                    <li>вс <a class="ml-3" href="#">добавить</a></li>
                </uL>
            </div>
            <div class="col-md-3">
                <label>{{__('thours.publish_schedule_on')}}</label>
            </div>
            <div class="col-md-9">
                <div class="d-flex ">
                    <select name="mounth" class="">
                        <option value="6">6 месяцев</option>
                    </select>
                    <p>вперед</p>
                </div>
                <small>lorem ipsum dolor sit am</small>
            </div>

            <div class="col-md-3">
                <label>{{__('thours.time_to_book')}}</label>
            </div>
            <div class="col-md-9">
                <div class="d-flex mb-0">
                    <select class="" name="brone_time">
                        <option value="0">выбрать</option>
                    </select>
                    <p>вперед</p>
                </div>
                <small>lorem ipsum dolor sit am</small>
            </div>

            <div class="col-md-3">
                <label>{{__('thours.break_between_excursions')}}</label>
            </div>
            <div class="col-md-9">
                <div class="d-flex mb-0">
                    <select class="" name="pause_time">
                        <option value="30">30 хв</option>
                    </select>
                    <p>вперед</p>
                </div>
                <small>lorem ipsum dolor sit am</small>
            </div>

            <div class="col-md-3">
                <label>{{__('thours.confirmation_of_orders')}}</label>
            </div>

            <div class="col-md-9">
                <ul class="">
                    <li class="d-flex"><input @if($tours->time_starts[0]->confirmation_order == "1") checked @endif type="radio" name="confirmation_order" value="1" class="mr-2"> <span class="mr-2">Lorem ipsum dolor sit amet consectetur adipisicing elit</span> <small>lorem ipsum dolor sit am</small></li>
                    <li class="d-flex"><input @if($tours->time_starts[0]->confirmation_order == "2") checked @endif type="radio" name="confirmation_order" value="2" class="mr-2"> <span class="mr-2">Lorem ipsum dolor sit amet consectetur adipisicing elit</span> <small>lorem ipsum dolor sit am</small></li>
                </ul>
            </div>
        </div>
    </section>




    <section>
        <div class="row col-md-12">
            <div class="d-flex justify-content-center">
                <h3>{{__('thours.start_point_excursion')}}</h3>
            </div>

            <div class="col-md-3">
                <label>{{__('thours.meeting_point')}}</label>
            </div>
            <div class="col-md-9">
                <input type="checkbox" value="1" name="place_tours">
                <small>lorem ipsum dolor sit</small>
            </div>

            <div class="col-md-3">
                <label>{{__('thours.approximately_meeting_point')}}</label>
            </div>
            <div class="col-md-9">
                <b>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo adipisci possimus ea nihil fugit, laudantium dignissimos nemo culpa eaque odio molestiae neque corporis, facere doloremque exercitationem cumque! Id, repellat fugiat?</b>
                <div class="bg-secondary row col-md-12 card">
                    <div class="">
                        {{__('thours.exact_meeting_point')}} <input type="text" value="{{$tours->place_tours[0]->place_city}}" name="place_city" placeholder="Киев"> мы вышлим после бронировки контакты гида

                    </div>
                </div>
            </div>

            <div class="col-md-3 mt-4">
                <label>{{__('thours.exact_meeting_point')}}</label>
            </div>
            <div class="col-md-9 mt-4">
                <div class="col-md-12 mb-2">
                    <b class="">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo adipisci possimus ea nihil fugit, laudantium dignissimos nemo culpa eaque odio molestiae neque corporis, facere doloremque exercitationem cumque! Id, repellat fugiat?</b>
                </div>
                <textarea name="meeting_point" class="form-control1" rows="5">{{$tours->place_tours[0]->meeting_point}}</textarea>
                <small>lorem ipsum dolor sit</small>
            </div>
        </div>
    </section>


    <section>
        <div class="row col-md-12">
            <div class="d-flex justify-content-center">
                <h3>{{__('thours.price')}}</h3>
            </div>
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laudantium similique deleniti qui doloremque voluptatum odio nulla, aspernatur perspiciatis numquam mollitia ipsum repellat quasi veritatis eos obcaecati commodi et, voluptates minima!
            </p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit</p>

            <div class="col-md-3">
                <label>{{__('thours.type_of_cost')}}</label>
            </div>
            <div class="col-md-9">
                <ul class="d-flex">
                    <li class="mr-2"><input @if($tours->price_tours[0]->type_price == 'tours') checked @endif type="radio" class="type_price" name="type_price" value="tours"> <label> {{__('thours.for_the_excursion')}}</label></li>
                    <li><input @if($tours->price_tours[0]->type_price == 'people') checked @endif type="radio" class="type_price" name="type_price" value="people"> <label> {{__('thours.per_person')}} </label></li>
                </ul>
            </div>

            <div class="col-md-3">
                <label>{{__('thours.price_option')}}</label>
            </div>
            <div class="col-md-8">
                <div class="d-flex">
                    <a href="#" class="mr-2">{{__('thours.single_price')}}</a>
                    <p> {{__('thours.prices_ticket_type')}}</p>
                </div>
            </div>

            <div class="col-md-3">
                <label>{{__('thours.discount')}}</label>
            </div>

            <div class="col-md-8">
                <div id="price">

                </div>
            </div>
            <div class="col-md-3">
                <label>{{__('thours.valid_until')}}</label>
            </div>

            <div class="col-md-8 ">

                <div class="d-flex row col-md-12">
                    <select class="col-md-3" name="discount" id="discount" >
                        <option value="0">Выбрать</option>
                        <option @if ($tours->price_tours[0]->discount == 10)
                            selected
                        @endif value="10">10%</option>
                    </select>
                    <p class="col-md-3">срок действия до </p>
                    <input type="date" name="discount_time" value="{{$tours->price_tours[0]->discount_time}}"  class="col-md-3 mr-2">
                    <a href="#" class="col-md-2">{{__('thours.cancel_discount')}}</a>
                </div>
                <small>Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur provident nobis, officia error reprehenderit soluta alias. Nemo, pariatur voluptas veniam saepe placeat sint adipisci rerum quisquam, sit esse suscipit doloremque?</small>
                <div id="discount-block">

                </div>
            </div>

        </div>
    </section>
    <section class="">
        <div class="row col-md-12 mt-2">

            <div class="col-md-6 d-flex">
                <label class="col-md-4">
                    {{__('thours.book_with')}}
                </label>
                <input type="datetime-local" value="{{$tours->start}}" name="start" class="form-control1">
            </div>
            <div class="col-md-6 d-flex">
                <label class="col-md-4">
                    {{__('thours.book_by')}}
                </label>
                <input type="datetime-local" name="end" {{$tours->end}} class="form-control1">
            </div>
        </div>
    </section>
    <section class="">
        <div class="row col-md-12">
            <div class="d-flex justify-content-center">
                <h3>{{__('thours.photos')}}</h3>
            </div>
            <p>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Laudantium similique deleniti qui doloremque voluptatum odio nulla, aspernatur perspiciatis numquam mollitia ipsum repellat quasi veritatis eos obcaecati commodi et, voluptates minima!
            </p>
            <input type="file" name="photo[]" multiple class="form-control1">
        </div>
    </section>

    <section class="mt-4">
        <div class="row col-md-12">
            <div class="d-flex justify-content-center">
                <h3>{{__('thours.About_the_guide')}}</h3>
            </div>
            <div class="col-md-3">
                <label class="">
                    {{__('thours.who_you_are')}}
                </label>
            </div>
            <div class="col-md-8">
                <ul>
                    <li><input type="radio" disabled checked> гид </li>
                    <li><input type="radio" disabled> група </li>
                    <li><input type="radio" disabled> агенство </li>
                </ul>
            </div>
        </div>
    </section>
    <div class="col-md-12 d-flex justify-content-center">
        <input type="submit" class="btn-primary btn" value="{{__('thours.send')}}">
    </div>
</form>
@endsection
