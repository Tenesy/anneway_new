@extends('welcome')
@section('content')
    <div class="mt-4 mb-4 container">
        <form method="post" enctype="multipart/form-data" action="/save-news">
            @csrf
            <div>
                <label>
                    Title
                </label>
                <input type="text" name="title" class="form-control" placeholder="Title">
            </div>
            <div>
                <label>
                    photo
                </label>
                <input type="file" multiple name="photo" class="form-control" placeholder="photo">
            </div>
            <div>
                <label>
                    description
                </label>
                <input type="text" name="description" class="form-control" placeholder="description">
            </div>
            <input type="submit" name="" value="создать" class="btn btn-primary mt-4">
        </form>
    </div>
@endsection
