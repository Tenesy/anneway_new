@extends('welcome')
@section('content')
<section id="blog-1col-sidebar" class="pt-50 pt-md-100 pb-md-50 light">
    <div class="container">
        @role('Admin')
        <a href="/create-news" class="btn btn-primary">создать новость</a>
        @endrole
        <div class="row">
            <div class="col-md-8 col-lg-9">
                @foreach ($news as $new)
                    <div class="gallery-item gallery-style-1 padding-x2 mb-50 border">
                        <a href="#"><img class="item-img" src="@if ($new->image) {{$new->image}} @else https://via.placeholder.com/1024x300 @endif" alt="image"></a>
                        <div class="item-title">
                            <h4 class=""><strong>{{$new->title}}</strong></h4>
                            <p class="text-secondary small">14 Feb 2018 | <a href="#">Sarah Doe</a></p>
                            <p class="">{{$new->description}}</p>
                        </div>
                        <div class="item-icon">
                            <ul class="list-inline small">
                                <li class=""><svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 16 16" width="16" class="icon svg-secondary icon-pos-left"><path d="m14 10.7994646v5.2005354l-4.64026195-3.093508c-.44192631.0614803-.89622881.093508-1.35973805.093508-4.418278 0-8-2.9101491-8-6.5 0-3.58985087 3.581722-6.5 8-6.5s8 2.91014913 8 6.5c0 1.64847552-.7552732 3.15362319-2 4.2994646z" fill-rule="evenodd"></path></svg><a href="#"><strong>26</strong></a></li>
                                <li class=""><svg xmlns="http://www.w3.org/2000/svg" height="16px" viewBox="0 0 16 16" width="16" class="icon svg-secondary icon-pos-left"><path d="m14.3206469 9.25651754-6.12825874 6.12825876-6.12825877-6.12825876c-1.69227194-1.69227193-1.69227194-4.43598684 0-6.12825877 1.69227193-1.69227193 4.43598683-1.69227193 6.12825877 0 1.69227193-1.69227193 4.43598684-1.69227193 6.12825874 0 1.692272 1.69227193 1.692272 4.43598684 0 6.12825877z"></path></svg><a href="#"><strong>14</strong></a></li>
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
            <aside class="col-md-4 col-lg-3">
                <form action="scripts/request.php" class="contact_form form-inline mb-50" id="blog-1col-sidebar-form" novalidate="novalidate">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Поиск" name="SEARCH" size="10"></div>
                    <button type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-link"><svg xmlns="http://www.w3.org/2000/svg" height="16" viewBox="0 0 16 16" width="16" class="icon svg-default"><path d="m12.4199285 10.8391071 3.2713969 3.2713969c.4115661.4115661.4115661 1.0788466 0 1.4904127s-1.0788466.4115661-1.4904127 0l-3.2573956-3.2573956c-1.14199306.8523301-2.55871405 1.3569312-4.09329095 1.3569312-3.78327543 0-6.85022615-3.0669507-6.85022615-6.85022615 0-3.78327543 3.06695072-6.85022615 6.85022615-6.85022615 3.78327545 0 6.85022615 3.06695072 6.85022615 6.85022615 0 1.48814524-.4745276 2.86545861-1.2805238 3.98888095zm-5.56970235.7535833c2.61919069 0 4.74246425-2.12327356 4.74246425-4.74246425 0-2.61919068-2.12327356-4.74246426-4.74246425-4.74246426-2.61919068 0-4.74246426 2.12327358-4.74246426 4.74246426 0 2.61919069 2.12327358 4.74246425 4.74246426 4.74246425z"></path></svg></button>
                </form>

                <div class="content-box mb-50">
                    <h4 class=""><strong>Архив новостей</strong></h4>
                    <ul class="list-unstyled padding-x2-list separate-list small">
                        <li class=""><a href="#">January</a> <span class="float-right">14</span></li>
                        <li class=""><a href="#">February</a> <span class="float-right">14</span></li>
                        <li class=""><a href="#">March</a> <span class="float-right">14</span></li>
                        <li class=""><a href="#">April</a> <span class="float-right">14</span></li>
                        <li class=""><a href="#">May</a><span class="float-right">14</span></li>
                        <li class=""><a href="#">June</a><span class="float-right">14</span></li>
                        <li class=""><a href="#">July</a><span class="float-right">14</span></li>
                        <li class=""><a href="#">August</a><span class="float-right">14</span></li>
                        <li class=""><a href="#">September</a><span class="float-right">14</span></li>
                        <li class=""><a href="#">October</a><span class="float-right">14</span></li>
                        <li class=""><a href="#">November</a><span class="float-right">14</span></li>
                        <li class=""><a href="#">December</a><span class="float-right">14</span></li>
                    </ul>
                </div>

                <div class="content-box mb-50 padding-x2" style="background-image: url(images/envelope-bg.png); background-size: contain; background-position: center top; background-repeat: no-repeat; background-color: #f4f5f6;">
                    <h4 class=""><strong>Подписка на новости</strong></h4>
                    <form action="scripts/request.php" class="contact_form form-vertical mb-20" id="blog-1col-sidebar-form" novalidate="novalidate">
                        <div class="form-group"><input type="email" class="form-control" placeholder="Email Address*" name="EMAIL" size="10"></div>
                        <button type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-sm btn-primary mt-20 btn-block"><strong>Подписаться</strong></button>
                    </form>
                    <p class="small text-secondary mb-0">Вы сможете отказаться от рассылок в любое время.</p>
                </div>

                <div class="content-box mb-50">
                    <h4 class="mb-20"><strong>Облако тэгов</strong></h4>
                    <div class="btn-cloud">
                        <a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a><a href="#" class="btn btn-sm btn-outline-secondary"><strong>Tag title</strong></a>
                    </div>
                </div>

                <a href="#"><img class="mw-100 mb-50" src="https://via.placeholder.com/320x100" srcset="images/banner-336280@2x.jpg 2x" alt="banner"></a>

            </aside>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
@endsection
