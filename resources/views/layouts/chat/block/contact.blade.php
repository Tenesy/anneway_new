<div class="tab-pane" id="contact">
    <h5 class="font-size-14 px-3 mb-3">Contact</h5>

    <div data-simplebar="init" style="max-height: 345px;">
        <div class="simplebar-wrapper" style="margin: 0px;">
            <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
            </div>
            <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                    <div class="simplebar-content-wrapper"
                        style="height: auto; overflow: hidden;">
                        <div class="simplebar-content" style="padding: 0px;">
                            <div>
                                <ul class="list-unstyled chat-list">
                                    @foreach ($users as $user)
                                        <li id="chat-user" data-name_chat="{{\App\Models\ChatUser::where('name_chat', 'like', '%'.\Auth::user()->id.'%')->where('name_chat', 'like', '%'.$user->id.'%')->first()->name_chat ?? 'oneTone_'.\Auth::user()->id.$user->id}}" data-user_name="{{$user->name}}" data-user_id="{{$user->id}}">
                                            <a href="#">
                                                <h5 class="font-size-14 mb-0">{{$user->name}}
                                                </h5>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="simplebar-placeholder" style="width: 0px; height: 0px;"></div>
        </div>
        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
            <div class="simplebar-scrollbar"
                style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
        </div>
        <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">
            <div class="simplebar-scrollbar"
                style="transform: translate3d(0px, 0px, 0px); display: none; height: 198px;">
            </div>
        </div>
    </div>

</div>
