<div class="tab-pane " id="group">
    <h5 class="font-size-14 px-3 mb-3">Чат-группы</h5>
    <ul class="list-unstyled chat-list" data-simplebar="init"
        style="max-height: 345px;">
        <div class="simplebar-wrapper" style="margin: 0px;">
            <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
            </div>
            <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                    <div class="simplebar-content-wrapper"
                        style="height: auto; overflow: hidden;">
                        <div class="simplebar-content" style="padding: 0px;">
                            <li class="group" data-group="General">
                                <a href="#">
                                    <div class="media align-items-center">
                                        <div class="avatar-xs mr-3">
                                            <span
                                                class="avatar-title rounded-circle bg-light text-body">
                                                G
                                            </span>
                                        </div>

                                        <div class="media-body">
                                            <h5 class="font-size-14 mb-0">General</h5>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </div>
                    </div>
                </div>
            </div>
            <div class="simplebar-placeholder" style="width: 0px; height: 0px;"></div>
        </div>
        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
            <div class="simplebar-scrollbar"
                style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
        </div>
        <div class="simplebar-track simplebar-vertical" style="visibility: hidden;">
            <div class="simplebar-scrollbar"
                style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
        </div>
    </ul>
</div>
