@extends('welcome')
@section('content')

<form class="row mb-4 mt-4 ml-4 mr-4">
    <input type="text" class="form-control mb-4 mt-4 ml-4 mr-4" name="name" @if (Auth::user()) value="{{Auth::user()->first_name}} {{Auth::user()->first_name}}"@else value="гость" @endif readonly>
    <textarea class="form-control mb-4 mt-4 ml-4 mr-4" id="text" name="text" rows="5" placeholder="комент"></textarea>
    <input type="submit" id="send" value="отправить" class="btn btn-primary">
</form>




<script src="https://cdn.socket.io/4.1.2/socket.io.min.js" integrity="sha384-toS6mmwu70G0fw54EGlWWeA4z3dyJ+dlXBtSURSKN4vyRFOcxd3Bzjj/AoOwY+Rg" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    let ip_address = '127.0.0.1';
    let socket_port = 3000;
    let socket = io("http://127.0.0.1:3000");

    $(document).on('click', '#send', function(e){
        e.preventDefault();
        let data = {
            name: $('input[name="name"]').val(),
            text: $('#text').val(),
        }
        socket.emit('publick_add_message', data);
    });

    socket.on('publick_new_message', function(data) {
        console.log(data);
    });

    socket.on('publick_all_message', function(data) {
        console.log(data);
    });

</script>
@endsection
