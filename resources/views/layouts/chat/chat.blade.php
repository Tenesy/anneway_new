@extends('welcome')
@section('content')
<main>
    <section id="inner-post-sidebar" class="pt-30 pt-md-70 pb-md-30 light">
        <div class="container">
            <h4 class="mb-4 mt-1"><strong>Чат с пользователями</strong></h4>
        </div>
        <div class="container">
            <div class="d-lg-flex mb-4">
                <div class="chat-leftsidebar">
                    <div class="p-3 border-bottom">
                        <div class="media">
                            <div class="align-self-center mr-3">
                                <img src="/images/users/avatar-2.jpg" class="avatar-xs rounded-circle" alt="">
                            </div>
                            <div class="media-body">
                                <h5 class="font-size-15 mt-0 mb-1">{{\Auth::user()->name}}</h5>
                                <p class="text-muted mb-0"><i
                                        class="mdi mdi-circle text-success align-middle mr-1"></i> В сети</p>
                            </div>

                            <div>
                                <div class="dropdown chat-noti-dropdown">
                                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-dots-horizontal font-size-20"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" style="">
                                        <a class="dropdown-item" href="#">Настройки чата</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body border-bottom py-2">
                        <div class="search-box chat-search-box">
                            <div class="position-relative">
                                <input type="text" class="form-control" placeholder="Поиск по чатам...">
                                <i class="ri-search-line search-icon"></i>
                            </div>
                        </div>
                    </div>

                    <div class="chat-leftsidebar-nav">
                        <ul class="nav nav-pills nav-justified">
                            <li class="nav-item">
                                <a href="#chat" data-toggle="tab" aria-expanded="true" class="nav-link active">
                                    <i class="ri-message-2-line font-size-20"></i>
                                    <span class="mt-2 d-none d-sm-block">Личный</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#group" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="ri-group-line font-size-20"></i>
                                    <span class="mt-2 d-none d-sm-block">Группы</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#contact" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    <i class="ri-contacts-book-2-line font-size-20"></i>
                                    <span class="mt-2 d-none d-sm-block">Контакты</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content py-4">
                        <div class="tab-pane show" id="chat">
                            <div>
                                <h5 class="font-size-14 px-3 mb-3">Последние сообщения</h5>
                                <ul class="list-unstyled chat-list" data-simplebar="init"
                                    style="max-height: 345px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -15px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper"
                                                    style="height: auto; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">
                                                        @foreach ($list_user as $list)
                                                        <li class="active" id="chat-user"  data-name_chat="{{\App\Models\ChatUser::where('name_chat', 'like', '%'.\Auth::user()->id.'%')->where('name_chat', 'like', '%'.$list->id.'%')->first()->name_chat ?? 'oneTone_'.\Auth::user()->id.$list->id}}" data-user_name="{{$list->name}}" data-user_id="{{$list->id}}">
                                                            <a href="#">
                                                                <div class="media">
                                                                    <div
                                                                        class="user-img online align-self-center mr-3">
                                                                        <img src="/images/users/avatar-2.jpg"
                                                                            class="rounded-circle avatar-xs"
                                                                            alt="">
                                                                        <span class="user-status"></span>
                                                                    </div>
                                                                    <div class="media-body overflow-hidden">
                                                                        <h5
                                                                            class="text-truncate font-size-14 mb-1">
                                                                            {{$list->name}}</h5>
                                                                        {{-- <p class="text-truncate mb-0">Hey! there
                                                                            I'm available</p> --}}
                                                                    </div>
                                                                    {{-- <div class="font-size-11">04 min</div> --}}
                                                                </div>
                                                            </a>
                                                        </li>
                                                        @endforeach
                                                        {{-- <li>
                                                            <a href="#">
                                                                <div class="media">
                                                                    <div class="user-img online mr-3">
                                                                        <div
                                                                            class="avatar-xs align-self-center">
                                                                            <span
                                                                                class="avatar-title rounded-circle bg-light text-body">
                                                                                K
                                                                            </span>
                                                                        </div>
                                                                        <span class="user-status"></span>
                                                                    </div>

                                                                    <div class="media-body overflow-hidden">
                                                                        <h5
                                                                            class="text-truncate font-size-14 mb-1">
                                                                            Tracy Penley</h5>
                                                                        <p class="text-truncate mb-0">This theme
                                                                            is awesome!</p>
                                                                    </div>
                                                                    <div class="font-size-11">24/03</div>
                                                                </div>
                                                            </a>
                                                        </li> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 490px;">
                                        </div>
                                    </div>
                                    <div class="simplebar-track simplebar-horizontal"
                                        style="visibility: hidden;">
                                        <div class="simplebar-scrollbar simplebar-visible"
                                            style="transform: translate3d(0px, 0px, 0px); display: none;"></div>
                                    </div>
                                    <div class="simplebar-track simplebar-vertical"
                                        style="visibility: visible;">
                                        <div class="simplebar-scrollbar simplebar-visible"
                                            style="height: 242px; transform: translate3d(0px, 0px, 0px); display: block;">
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </div>

                        @include('layouts.chat.block.group')

                        @include('layouts.chat.block.contact')
                    </div>
                </div>

                @include('layouts.chat.block.chat')
            </div>
        </div>
    </section>
</main>
@section('js')
<script src="{{asset('js/libs/simplebar/simplebar.min.js')}}"></script>


<script src="https://cdn.socket.io/4.1.2/socket.io.min.js" integrity="sha384-toS6mmwu70G0fw54EGlWWeA4z3dyJ+dlXBtSURSKN4vyRFOcxd3Bzjj/AoOwY+Rg" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>

<script>
    let ip_address = '127.0.0.1';
    let socket_port = 3000;
    let socket = io("http://127.0.0.1:3000");
    let name = "{{\Auth::user()->last_name . \Auth::user()->name}}";
    let group = 'General';
    var options = {
        timezone: 'UTC',
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
    };
    var user_id = "{{\Auth::user()->id}}"

    $(document).on('click', '#chat-user', function (e) {
        $('#message-group').html('');
        e.preventDefault();
        let chat_user_id = $(this).data('user_id');
        let li = this;
        $('#chat_name').text($(this).data('user_name'));
        group = $(this).data('name_chat') ? $(this).data('name_chat') : 'oneTone_'+user_id+''+chat_user_id;
        socket.emit('chat_user', {
            user_id: user_id,
            from_user:chat_user_id,
            name: $(this).data('name_chat'),
        });

        socket.on('get_chat_user', function(data) {
            if (data.length) {
                data.forEach(element => {
                    if (element.user_id == user_id) {
                        $('#message-group').append(
                        `
                            <li class="right">
                                <div class="conversation-list">
                                    <div class="ctext-wrap">
                                        <div class="conversation-name">${element.name}</div>
                                        <div class="ctext-wrap-content">
                                            <p class="mb-0">
                                                ${element.text}
                                            </p>
                                        </div>
                                        <p class="chat-time mb-0"><i
                                                class="bx bx-time-five align-middle mr-1"></i>
                                                ${new Date(element.createdAt).toLocaleString("ru", options)}</p>
                                    </div>
                                </div>
                            </li>
                        `);
                    } else {
                        $('#message-group').append(
                            `
                                <li>
                            <div class="conversation-list">
                                <div class="chat-avatar">
                                    <img src="/images/users/avatar-2.jpg" alt="">
                                </div>
                                <div class="ctext-wrap">
                                    <div class="conversation-name">${element.name}
                                    </div>
                                    <div class="ctext-wrap-content">
                                        <p class="mb-0">
                                            ${element.text}
                                        </p>
                                    </div>
                                    <p class="chat-time mb-0"><i
                                            class="mdi mdi-clock-outline align-middle mr-1"></i>
                                        ${new Date(element.createdAt).toLocaleString("ru", options)}</p>
                                </div>
                            </div>
                        </li>
                        `)
                    }
                });
            } else {
                $(li).attr('data-name_chat',data.name_chat);
            }
        })

    });


    $('#chat_name').text(group);
    $(document).on('click', '.group', function (e) {
        e.preventDefault();
        group = $(this).data('group');
        $('#chat_name').text(group);
        socket.emit('publick_all_message', group);
    });

    $(document).on('click', '#send', function(e){
        e.preventDefault();
        let data = {
            name: "{{\Auth::user()->name}}",
            group,
            user_id,
            image: "{{\Auth::user()->photo}}",
            text: $('#text').val(),
        }
        socket.emit('publick_add_message', data);
    });

    socket.on('publick_new_message', function(element) {
        console.log(element);
            if (element.user_id === user_id) {
                $('#message-group').append(
                    `
                        <li class="right">
                    <div class="conversation-list">
                        <div class="ctext-wrap">
                            <div class="conversation-name">${element.name}</div>
                            <div class="ctext-wrap-content">
                                <p class="mb-0">
                                    ${element.text}
                                </p>
                            </div>
                            <p class="chat-time mb-0"><i
                                    class="bx bx-time-five align-middle mr-1"></i>
                                    ${new Date(element.createdAt).toLocaleString("ru", options)}</p>
                        </div>
                    </div>
                </li>
                `)
            } else {
                $('#message-group').append(
                    `
                        <li>
                    <div class="conversation-list">
                        <div class="chat-avatar">
                            <img src="${element.image}" alt="">
                        </div>
                        <div class="ctext-wrap">
                            <div class="conversation-name">${element.name}
                            </div>
                            <div class="ctext-wrap-content">
                                <p class="mb-0">
                                    ${element.text}
                                </p>
                            </div>
                            <p class="chat-time mb-0"><i
                                    class="mdi mdi-clock-outline align-middle mr-1"></i>
                                ${new Date(element.createdAt).toLocaleString("ru", options)}</p>
                        </div>
                    </div>
                </li>
                `)
            }
    });

    socket.emit('publick_all_message', group);

    socket.on('publick_all_message', function(data) {
        data.forEach(element => {
            if (element.user_id == user_id) {
                $('#message-group').append(
                `
                        <li class="right">
                    <div class="conversation-list">
                        <div class="ctext-wrap">
                            <div class="conversation-name">${element.name}</div>
                            <div class="ctext-wrap-content">
                                <p class="mb-0">
                                    ${element.text}
                                </p>
                            </div>
                            <p class="chat-time mb-0"><i
                                    class="bx bx-time-five align-middle mr-1"></i>
                                    ${new Date(element.createdAt).toLocaleString("ru", options)}</p>
                        </div>
                    </div>
                </li>
                `);
            } else {
                $('#message-group').append(
                    `
                        <li>
                    <div class="conversation-list">
                        <div class="chat-avatar">
                            <img src="/images/users/avatar-2.jpg" alt="">
                        </div>
                        <div class="ctext-wrap">
                            <div class="conversation-name">${element.name}
                            </div>
                            <div class="ctext-wrap-content">
                                <p class="mb-0">
                                    ${element.text}
                                </p>
                            </div>
                            <p class="chat-time mb-0"><i
                                    class="mdi mdi-clock-outline align-middle mr-1"></i>
                                ${new Date(element.createdAt).toLocaleString("ru", options)}</p>
                        </div>
                    </div>
                </li>
                `)
            }
        });
        // console.log(data);
    });

</script>
@endsection
@endsection
