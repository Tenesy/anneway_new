@extends('welcome')

@section('content')
<section id="subscribe-block" class="pt-50 pb-50 pt-md-100 pb-md-100 text-center light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
                <div class="content-box padding-x3 bg-default shadow" style="">
                    <h3 class="mb-20">Востановления пароля</h3>
                    <form method="POST" action="/pass" class="contact_form form-vertical mb-30" id="subscribe-block-form" novalidate="novalidate">
                        <div class="form-group text-field-group"><input required type="password" class="form-control" placeholder="Пароль" name="password" size="10" required></div>
                        <input value="{{$email}}" type="hidden" class="form-control" placeholder="email" name="email" required>
                        <div class="form-group email-field-group"><input required type="password" class="form-control" placeholder="повторить пароль" name="password_confirmation" size="10" required></div>
                        @csrf
                        <button type="submit" data-loading-text="•••" data-complete-text="Completed!" data-reset-text="Try again later..." class="btn btn-primary mt-30"><strong>Войти</strong></button>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>

@endsection
