@extends('welcome')
@section('content')
<section id="subscribe-block" class="pt-50 pb-50 pt-md-100 pb-md-100 text-center light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ml-auto mr-auto">
                <div class="content-box padding-x3 bg-default shadow" style="box-shadow: 0px 8px 20px 0px rgb(0 0 0 / 20%);">
                    <h3 class="mb-20">@if (!$err){{__('index.check_email')}}@endif</h3>
                    <p class="text-secondary">@if (!$err){{__('index.link_email')}} {{$email}}@else<span style="font-size:20px">{{$err}}</span>  @endif</p>
                    @if ($err)<p style="font-size:20px" class="text-primary"><a class="col-md-6" href="/login">вход</a> <a class="col-md-6" href="/register">регистрация</a></p>@endif
                </div>
            </div>
        </div>
    </div>
    <div class="bg-wrap">
        <div class="bg"></div>
    </div>
</section>
@endsection
