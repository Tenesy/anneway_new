<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TimeStart extends Model
{


    protected $fillable = [
        'type_tours',
        'monday_start',
        'monday_end',
        'tuesday_start',
        'tuesday_end',
        'wednesday_start',
        'wednesday_end',
        'thursday_start',
        'thursday_end',
        'friday_start',
        'friday_end',
        'saturday_start',
        'saturday_end',
        'sunday_start',
        'sunday_end',
        'mounth',
        'minimum_booking_time',
        'maximum_booking_time',
        'brone_time',
        'pause_time',
        'confirmation_order',
        'tour_id',
    ];
}
