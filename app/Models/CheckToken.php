<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckToken extends Model
{


    protected $fillable = [
        'token',
        'user_id',
    ];

    public function users () {
        return $this->hasOne(\App\Models\User::class, 'id', 'user_id');
    }
}
