<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ToursConditions extends Model
{


    protected $fillable = [
        'city',
        'language',
        'duration',
        'count_user',
        'children',
        'tour_id',
    ];
}
