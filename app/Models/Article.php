<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ArticleCategory;

class Article extends Model
{


    protected $fillable = [
        'title',
        'description',
        'image',
        'category_id',
    ];

    public function category () {
        return $this->hasOne(ArticleCategory::class, 'id', 'category_id');
    }
}
