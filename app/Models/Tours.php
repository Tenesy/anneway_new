<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ToursConditions;
use App\Models\ToursPhoto;
use App\Models\TimeStart;
use App\Models\PlaceTours;
use App\Models\PriceTours;
use App\Models\User;


class Tours extends Model
{


    protected $fillable = [
        'title',
        'description',
        'user_id',
        'details',
        'city',
        'language',
        'duration',
        'count_user',
        'children',
        'status',
        'start',
        'end',
        'user_id',
        'transport',
    ];


    public function user () {
        return $this->hasOne(User::class , 'id', 'user_id');
    }
    public function conditions() {
        return $this->hasMany(ToursConditions:: class, 'tour_id', 'id');
    }


    public function photos () {
        return $this->hasMany(ToursPhoto::class, 'tour_id', 'id');
    }

    public function time_starts () {
        return $this->hasMany(TimeStart::class, 'tour_id', 'id');
    }

    public function place_tours () {
        return $this->hasMany(PlaceTours::class, 'tour_id', 'id');
    }

    public function price_tours () {
        return $this->hasMany(PriceTours::class, 'tour_id', 'id');
    }
}
