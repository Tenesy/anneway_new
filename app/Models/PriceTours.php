<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceTours extends Model
{


    protected $fillable =[
        'type_price',
        'price_tours',
        'price_human1',
        'price_human2',
        'discount',
        'discount_time',
        'tour_id',
    ];
}
