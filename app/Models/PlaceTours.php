<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlaceTours extends Model
{


    protected $fillable = [
        'place_tours',
        'place_city',
        'meeting_point',
        'tour_id',
    ];
}
