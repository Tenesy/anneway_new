<?php

namespace App\Http\Controllers;
use App\Models\ArticleCategory;
use App\Models\Article;
use App\Models\News;
use Storage;
use Illuminate\Http\Request;
use Image;

class ArticleController extends Controller
{
    public function create_articles() {
        $categorys = ArticleCategory::all();
        return view('layouts.blog.create-blog', ['categorys' => $categorys]);

    }
    public function createDir($file)
    {
        \File::makeDirectory($file, $mode = 0777, true, true);
    }

    public function save_articles(Request $request) {
        $file = $request->file('photo');



        $article = Article::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => '',
            'category_id' => $request->category,
        ]);
        if ($file) {
            $this->createDir(public_path('images/blog/'.$article->id.'/'));

            Image::make($file)->save('images/blog/'.$article->id.'/'.$file->getClientOriginalName());
            $article->update([
                'image' => 'images/blog/'.$article->id.'/'.$file->getClientOriginalName(),
            ]);
        }

        return redirect('/blogs');
    }

    public function all_articles(Request $request, $category_id = null) {
        $categorys = ArticleCategory::all();
        if (!$category_id) {
            $articles = Article::orderBy('id', 'desc')->paginate();
        } else {
            $articles = Article::where('category_id', $category_id)->orderBy('id', 'desc')->paginate();
        }

        return view('layouts.blog.blog', ['articles' => $articles,'categorys' => $categorys ]);
    }


    public function create_news () {
        return view('layouts.news.create-news');
    }

    public function save_news(Request $request) {
        $file = $request->file('photo');
        $new = News::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => '',
        ]);
        if ($file) {
            $this->createDir(public_path('images/new/'.$new->id.'/'));

            Image::make($file)->save('images/new/'.$new->id.'/'.$file->getClientOriginalName());
            $new->update([
                'image' => 'images/new/'.$new->id.'/'.$file->getClientOriginalName(),
            ]);
        }

        return redirect('/news');
    }

    public function all_news() {
        $news = News::orderBy('id', 'desc')->paginate();

        return view('layouts.news.news', ['news'=> $news]);
    }
}
