<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tours;
use Auth;
use Illuminate\Support\Facades\Hash;
use Image;
class ToursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function info_tours ($id) {
        $tours = Tours::where('id', $id)->with('user', 'conditions', 'photos', 'time_starts', 'price_tours')->first();
        return view('layouts.tours.info', ['tours' => $tours]);
    }

    public function index()
    {
        $tours = Tours::where('user_id',  Auth::user()->id)->paginate();
        return view('layouts.tours.list', ['tours' => $tours]);
    }

    public function all_tours() {
        $tours = Tours::paginate();
        return view('layouts.tours.all-list', ['tours' => $tours]);
    }
    public function createDir($file)
    {
        \File::makeDirectory($file, $mode = 0777, true, true);
    }
    public function updates (Request $request, $id) {
        $tours = Tours::find($id);
        $tours->update([
            'title' => $request->title,
            'description' => $request->description,
            'details' => $request->details,
            'city' => $request->city,
            'language' => $request->language,
            'duration' => $request->duration,
            'count_user' => $request->count_user,
            'children' => $request->children,
            'status' => $request->status,
            'start' => $request->start,
            'end' => $request->end,
            'user_id' => Auth::user() ? Auth::user()->id : 1,
        ]);
            if ($request['photo'] && count($request['photo'])) {
                foreach ($request['photo'] as $file) {
                    if ($file) {
                        $this->createDir(public_path('images/tours/'.$tours->id.'/'));

                        Image::make($file)->save('images/tours/'.$tours->id.'/'.$file->getClientOriginalName());
                        $tours->photos()->create([
                            'photo' => 'images/tours/'.$tours->id.'/'.$file->getClientOriginalName(),
                        ]);
                    }


                }
            }

        $tours->time_starts()->update([
            'type_tours' => $request->type_tours ? $request->type_tours : 0,
            'monday_start' => $request->monday_start ? $request->monday_start : 0,
            'monday_end' => $request->monday_end ? $request->monday_end : 0,
            'tuesday_start' => $request->tuesday_start ? $request->tuesday_start : 0,
            'tuesday_end' => $request->tuesday_end ? $request->tuesday_end : 0,
            'wednesday_start' => $request->wednesday_start ? $request->wednesday_start : 0,
            'wednesday_end' => $request->wednesday_end ? $request->wednesday_end : 0,
            'thursday_start' => $request->thursday_start ? $request->thursday_start : 0,
            'thursday_end' => $request->thursday_end ? $request->thursday_end : 0,
            'friday_start' => $request->friday_start ? $request->friday_start : 0,
            'friday_end' => $request->friday_end ? $request->friday_end : 0,
            'mounth' => $request->mounth ? $request->mounth : 0,
            'brone_time' => $request->brone_time ? $request->brone_time : 0,
            'pause_time' => $request->pause_time ? $request->pause_time : 0,
            'confirmation_order' => $request->confirmation_order ? $request->confirmation_order : 0,
        ]);

        $tours->place_tours()->update([
            'place_tours' => $request->place_tours,
            'place_city' => $request->place_city,
            'meeting_point' => $request->meeting_point,
        ]);


        $tours->conditions()->update([
            'city' => $request->city,
            'language' => $request->language,
            'duration' => $request->duration,
            'count_user' => $request->count_user,
            'children' => $request->children,
        ]);

        $tours->price_tours()->update([
            'type_price' => $request->type_price,
            'price_tours' => $request->price_tours,
            'price_human1' => $request->price_human1,
            'price_human2' => $request->price_human2,
            'discount' => $request->discount,
            'discount_time' => $request->discount_time,
        ]);
        return redirect()->route('tours.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        return view('layouts.tours.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tours = Tours::create([
            'title' => $request->title,
            'description' => $request->description,
            'details' => $request->details,
            'city' => $request->city,
            'language' => $request->language,
            'duration' => $request->duration,
            'count_user' => $request->count_user,
            'children' => $request->children,
            'status' => $request->status,
            'start' => $request->start,
            'end' => $request->end,
            'transport' => $request->transport,
            'user_id' => Auth::user()->id,
        ]);

        if ($request['photo']) {
            foreach ($request['photo'] as $file) {

                if ($file) {
                    $this->createDir(public_path('images/tours/'.$tours->id.'/'));

                    Image::make($file)->save('images/tours/'.$tours->id.'/'.$file->getClientOriginalName());
                    $tours->photos()->create([
                        'photo' => 'images/tours/'.$tours->id.'/'.$file->getClientOriginalName(),
                    ]);
                }
            }
        }


        $tours->time_starts()->create([
            'type_tours' => $request->type_tours ? $request->type_tours : 0,
            'monday_start' => $request->monday_start ? $request->monday_start : 0,
            'monday_end' => $request->monday_end ? $request->monday_end : 0,
            'tuesday_start' => $request->tuesday_start ? $request->tuesday_start : 0,
            'tuesday_end' => $request->tuesday_end ? $request->tuesday_end : 0,
            'wednesday_start' => $request->wednesday_start ? $request->wednesday_start : 0,
            'wednesday_end' => $request->wednesday_end ? $request->wednesday_end : 0,
            'thursday_start' => $request->thursday_start ? $request->thursday_start : 0,
            'thursday_end' => $request->thursday_end ? $request->thursday_end : 0,
            'friday_start' => $request->friday_start ? $request->friday_start : 0,
            'friday_end' => $request->friday_end ? $request->friday_end : 0,
            'saturday_start' => $request->saturday_start ? $request->saturday_start : 0,
            'saturday_end' => $request->saturday_end ? $request->saturday_end : 0,
            'sunday_start' => $request->sunday_start ? $request->sunday_start : 0,
            'sunday_end' => $request->sunday_end ? $request->sunday_end : 0,
            'mounth' => $request->mounth ? $request->mounth : 0,
            'minimum_booking_time' => $request->minimum_booking_time ? $request->minimum_booking_time : 0,
            'maximum_booking_time' => $request->maximum_booking_time ? $request->maximum_booking_time : 0,
            'confirmation_order' => $request->confirmation_order ? $request->confirmation_order : 0,
        ]);

        $tours->place_tours()->create([
            'place_tours' => $request->place_tours,
            'place_city' => $request->place_city,
            // 'meeting_point' => $request->meeting_point,
        ]);


        $tours->conditions()->create([
            'city' => $request->city,
            'language' => $request->language,
            'duration' => $request->duration,
            'count_user' => $request->count_user,
            'children' => $request->children,
        ]);

        $tours->price_tours()->create([
            // 'type_price' => $request->type_price,
            'price_tours' => $request->price_tours,
            'price_human1' => $request->price_human1,
            // 'price_human2' => $request->price_human2,
            'discount' => $request->discount,
            // 'discount_time' => $request->discount_time,
        ]);
        return redirect()->route('tours.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Tours::where('id', $id)->delete();
        return redirect()->route('tours.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tours = Tours::find($id);
        return view('layouts.tours.edit', ['tours' => $tours]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
