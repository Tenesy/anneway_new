<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Resetpassword;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class CustomRegController extends Controller
{
    public function reset_password (Request $request, $token = null) {
        if (!isset($_COOKIE['token']) ) {
            setcookie('token', uniqid());

        }
        if (!isset($_COOKIE['email']) ) {
            setcookie('email', uniqid());

        }

        if (session()->get('token') &&  $token == session()->get('token') ) {
            return view('layouts.pass', ['email' => session()->get('email')]);
        } else {
            $token = Str::random(40);
            Mail::to($request->email)->send(new Resetpassword($token));

            session()->put('token', $token);
            session()->put('email', $request->email);
        }

        return view('layouts.info-block', ['email' => $request->email]);
    }

    public function pass (Request $request) {
        $request->validate([
            'password' => 'required|
                            min:6|confirmed',
        ]);
        User::where('email', $request->email)->first()->update(['password'=> Hash::make($request->password)]);
        if (\Auth::attempt($request->only('email', 'password')))  {
            return redirect('/cabinet');
        }
    }
}
