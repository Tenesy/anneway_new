<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Mail\ConfirmationMails;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\CheckToken;
use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/info';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'parent_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        // dd($data);

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'last_name' => $data['last_name'],
            'first_name' => $data['name'],
            'parent_name' => $data['parent_name'],
        ]);

        $user->assignRole($data['role']);
        $user->update(['status_register' => 0]);
        $token = Str::random(40);
        Mail::to($data['email'])->send(new ConfirmationMails($token));
        $user->token()->create([
            'token' => $token,
        ]);
        if (!isset($_COOKIE['email']) ) {
            setcookie('email', uniqid());

        }
        session()->put('email', $user->email);
        return $user;
    }

    public function check (Request $request) {
        $check_token = CheckToken::where('token', $request->token)->first();
        if (isset($check_token)) {
            // Auth::logout();
            $check_toke = Auth::loginUsingId($check_token->user_id);
            if (!$check_token->users->can('guide')) {
                $check_token->users->assignRole('guide');
            }
            $check_token->users->update(['status_register' => 1]);
            $check_token->delete();
            return redirect('/');
        } else {
            dd('токен не дествителен');
        }

    }
}
