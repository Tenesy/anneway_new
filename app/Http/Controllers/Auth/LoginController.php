<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Mail\ConfirmationMails;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/cabinet';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login_check_role (Request $request) {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        if (Auth::attempt($request->only('email', 'password'))) {
            if (Auth::user()->two_auth) {

                Auth::logout();
                $user = User::where(['email' => $request->email])->first();
                $token = Str::random(40);

                Mail::to($request->email)->send(new ConfirmationMails($token));
                $user->token()->create([
                    'token' => $token,
                    'user_id' => $user->id,
                ]);
                return view('layouts.info-block', ['email' => $request->email, 'err' => null]);


            } else {
                return redirect('/cabinet');
            }

        }
        return view('layouts.info-block', ['err' => 'пользователь не зарегистрирован']);
    }
}
