<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Storage;
use Sendpulse\RestApi\ApiClient;
use Sendpulse\RestApi\Storage\FileStorage;
use Illuminate\Support\Facades\App;
use App\Models\Notifications;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Image;
class CabinetController extends Controller
{
    public function index () {
        $notifications = Notifications::all();
        return view('layouts.cabinet', ['user' => Auth::user(), 'notifications' => $notifications]);
    }

    public function update_user (Request $request) {
        $validated = $request->validate([
            'last_name' => 'required',
            'first_name' => 'required',
            'parent_name' => 'required',
        ]);

        Auth::user()->update($validated);

        if (isset($request->photo)) {
            $file = $request->file('photo');
            $photo = Auth::user()->photo;
            if ($file) {
                $img = Image::make($file);
                $img->resize(520, 520, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                \File::makeDirectory(public_path('images/users/'.Auth::user()->id.'/'), $mode = 0777, true, true);
                $img->save(public_path('images/users/'.Auth::user()->id.'/'.$file->getClientOriginalName()));

            }

            Auth::user()->update(['photo_url' => '/images/users/'.Auth::user()->id.'/'.$file->getClientOriginalName()]);
        }
        return redirect()->back();
        // dd( Auth::user());
    }

    public function generate_code (Request $request) {

        $request->validate([
            'phone' => 'required|max:12',
        ]);
        if (isset($request->t) && Carbon::parse( Auth::user()->telephones[0]->updated_at)->floatDiffInMinutes(Carbon::now())  < 120000) {
            $time = Carbon::parse( Auth::user()->telephones[0]->updated_at)->floatDiffInSeconds(Carbon::now());
            return ['status' => 'not_time', 'time' => $time];
        }
        // $SPApiClient = new ApiClient('d71fc86cb6278772f8572fb3875f8666', 'a40dd8e6728bb3831034baa6ac4eb4ea', new FileStorage());

        $a = range(0, 9);
        for ($i = 0, $n = count($a); $i < 4 && --$n; ++$i) {
            $r = mt_rand(0, $n);
            if ($r != $n) {
                $tmp = $a[$n];
                $a[$n] = $a[$r];
                $a[$r] = $tmp;
            }
        }
        $code = join('', array_slice($a, -4));

        $phones = [$request->phone];
        $params = [
            'sender' => 'ANewWay',
            'body' => 'code:'.$code,
        ];
        $additionalParams = [
            'transliterate' => 0
        ];
        if (!count(Auth::user()->telephones) || (count(Auth::user()->telephones) && !Auth::user()->telephones()->where('phone', $request->phone)->first())) {
            Auth::user()->telephones()->create([
                'phone' => $request->phone,
                'status' => 0,
                'code' => $code,
            ]);
        } else {
            Auth::user()->telephones()->update([
                'phone' => $request->phone,
                'status' => 0,
                'code' => $code,
            ]);
        }
        // $SPApiClient->sendSmsByList($phones, $params, $additionalParams);
        return $code;
    }

    public function check_code (Request $request) {
        $check = Auth::user()
                        ->telephones()
                        ->where('phone', $request->phone)
                        ->where('code', $request->code)
                        ->first();
        if ($check) {
            $check->update(['status' => 1]);
            return ['status' => 'verify'];
        }
        return ['status' => 'no_veiufy'];
    }

    public function contacts (Request $request) {
        Auth::user()->update(['email' => $request->email]);

        if (!count(Auth::user()->telephones) || (count(Auth::user()->telephones) && !Auth::user()->telephones()->where('phone', $request->phone)->first())) {
            Auth::user()->telephones()->create([
                'phone' => $request->phone,
            ]);
        } else {
            Auth::user()->telephones()->update([
                'phone' => $request->phone,
            ]);
        }
        return redirect()->back();
    }

    public function description_profile (Request $request) {
        if (Auth::user()->descriptions) {
            Auth::user()->descriptions()->update([
                'description' => $request->description,
                'hort_description' => $request->small_desc,
            ]);
        } else {
            Auth::user()->descriptions()->create([
                'description' => $request->description,
                'hort_description' => $request->small_desc,
            ]);
        }

        return redirect()->back();
    }

    public function notifications (Request $request) {

        foreach ($request->notificatio as $key => $notificatio) {
            if (Auth::user()->notifications()->wherePivot('notification_id', $key)->first()) {
                Auth::user()->notifications()->updateExistingPivot($key, ['email'=> array_key_exists('email', $notificatio) ?  $notificatio['email'] : null, 'telegram' => array_key_exists('telegram', $notificatio) ? $notificatio['telegram'] : null, 'sms' => array_key_exists('sms', $notificatio) ?  $notificatio['sms'] : null ]);
            } else {
                Auth::user()->notifications()->attach($key, ['email'=> array_key_exists('email', $notificatio) ?  $notificatio['email'] : null, 'telegram' => array_key_exists('telegram', $notificatio) ? $notificatio['telegram'] : null, 'sms' => array_key_exists('sms', $notificatio) ?  $notificatio['sms'] : null ]);

            }
        }

        return redirect()->back();
    }

    public function reset_password (Request $request) {
        if ($request->password) {

            $request->validate(['password' => 'required|min:6', 'new_password' => 'required|min:6']);
            if (Hash::check($request->password, Auth::user()->password)) {
                Auth::user()->update(['password' => Hash::make($request->new_password), 'two_auth' => $request->dv]);
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors(['неверный старый пароль']);
            }
        } else {
            Auth::user()->update(['two_auth' => $request->dv]);
            return redirect()->back();
        }

    }
}
