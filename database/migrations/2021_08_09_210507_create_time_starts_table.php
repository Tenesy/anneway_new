<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeStartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_starts', function (Blueprint $table) {
           $table->increments('id');
            $table->unsignedBigInteger('tour_id')
            ->references('tour_id')
            ->on('tours');
            $table->string('type_tours')->nullable();
            $table->string('monday_start')->nullable();
            $table->string('monday_end')->nullable();
            $table->string('tuesday_start')->nullable();
            $table->string('tuesday_end')->nullable();
            $table->string('wednesday_start')->nullable();
            $table->string('wednesday_end')->nullable();
            $table->string('thursday_start')->nullable();
            $table->string('thursday_end')->nullable();
            $table->string('friday_start')->nullable();
            $table->string('friday_end')->nullable();
            $table->string('saturday_start')->nullable();
            $table->string('saturday_end')->nullable();
            $table->string('sunday_start')->nullable();
            $table->string('sunday_end')->nullable();
            $table->string('mounth')->nullable();
            $table->string('brone_time')->nullable();
            $table->string('pause_time')->nullable();
            $table->string('minimum_booking_time')->nullable();
            $table->string('maximum_booking_time')->nullable();
            $table->string('confirmation_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_starts');
    }
}
