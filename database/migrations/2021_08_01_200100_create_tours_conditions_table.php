<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours_conditions', function (Blueprint $table) {
           $table->increments('id');
            $table->string('city');
            $table->string('language')->nullable();
            $table->integer('duration')->nullable();
            $table->integer('count_user')->nullable();
            $table->boolean('children')->nullable()->nullable();
            $table->unsignedBigInteger('tour_id')
            ->references('tour_id')
            ->on('tours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours_conditions');
    }
}
