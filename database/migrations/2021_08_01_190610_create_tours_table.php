<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
           $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->unsignedBigInteger('user_id')
            ->references('user_id')
            ->on('users');
            $table->text('details');
            $table->string('city')->nullable();
            $table->string('language')->nullable();
            $table->string('duration')->nullable();
            $table->string('count_user')->nullable();
            $table->boolean('children')->nullable();
            $table->string('status')->nullable();
            $table->datetime('start')->nullable();
            $table->datetime('end')->nullable();
            $table->string('transport')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
