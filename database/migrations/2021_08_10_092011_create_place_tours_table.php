<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaceToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_tours', function (Blueprint $table) {
           $table->increments('id');
            $table->string('place_tours')->nullable();
            $table->string('place_city')->nullable();
            $table->string('meeting_point')->nullable();
            $table->unsignedBigInteger('tour_id')
            ->references('tour_id')
            ->on('tours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_tours');
    }
}
