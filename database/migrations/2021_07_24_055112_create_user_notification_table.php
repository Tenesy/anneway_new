<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_notification', function (Blueprint $table) {
           $table->increments('id');
            $table->unsignedBigInteger('user_id')
            ->references('user_id')
            ->on('users');
            $table->unsignedBigInteger('notification_id')
            ->references('notification_id')
            ->on('notifications');
            $table->boolean('email')->nullable();
            $table->boolean('telegram')->nullable();
            $table->boolean('sms')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_notification');
    }
}
