<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_tours', function (Blueprint $table) {
           $table->increments('id');
            $table->string('type_price')->nullable();
            $table->string('price_tours')->nullable();
            $table->string('price_human1')->nullable();
            $table->string('price_human2')->nullable();
            $table->string('discount')->nullable();
            $table->date('discount_time')->nullable();
            $table->unsignedBigInteger('tour_id')
            ->references('tour_id')
            ->on('tours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_tours');
    }
}
