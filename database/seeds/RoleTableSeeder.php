<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\Notifications;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'parent_name' => 'Admin',
            'password' => Hash::make('Admin')
            , 'is_active' => 1, 'is_system' => 1
        ]);
        $user->assignRole('Admin');
    }
}
