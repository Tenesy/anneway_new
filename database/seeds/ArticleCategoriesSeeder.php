<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ArticleCategory;
class ArticleCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ArticleCategory::create(['name' => 'Путеществия']);
        ArticleCategory::create(['name' => 'Личный опыт']);
        ArticleCategory::create(['name' => 'Обзор']);
        ArticleCategory::create(['name' => 'Гастротур']);
        ArticleCategory::create(['name' => 'Мысль']);
    }
}
