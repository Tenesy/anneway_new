<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Notifications;
class NotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notifications = new Notifications();
        $notifications->create([
            'name' => 'уведомления о новых лидых',
        ]);

        $notifications->create([
            'name' => 'уведомления о груповых екскурсиях',
        ]);

        $notifications->create([
            'name' => 'уведомления о входе с неза',
        ]);
    }
}
